/**
 * 
 */
package blindcode.springmvc.service;

import org.springframework.data.repository.CrudRepository;

import blindcode.springmvc.common.ContactUsMessage;

/**
 * @author David
 *
 */
public interface ContactUsMessageRepository extends CrudRepository<ContactUsMessage, String>{

}
