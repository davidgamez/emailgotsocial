/**
 * 
 */
package blindcode.springmvc.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.contextio.common.EmailAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import blindcode.springmvc.common.ContactUsMessage;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author David
 *
 */
@Service
public class PublicServiceImpl implements PublicService {

    private static Logger logger = LoggerFactory.getLogger(PublicServiceImpl.class);
    
    private Gson gson = new GsonBuilder().setPrettyPrinting().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();

    @Autowired
    NotificationService notificationService;
    
    @Autowired
    ContactUsMessageRepository contactUsMessageRepository;
    
    @Value("${contactus.from}")
    private String contactUsFrom;
    
    @Value("${contactus.fromName}")
    private String contactUsfromName;
    
    @Value("${contactus.to}")
    private String contactUsTo;
    
    @Value("${captcha.verify.url}")
    private String captchaVerifyUrl;
    
    @Value("${captcha.secret}")
    private String captchaSecret;
    
    /* (non-Javadoc)
     * @see blindcode.springmvc.service.PublicService#saveContactUsMessage(blindcode.springmvc.common.ContactUsMessage)
     */
    @Override
    public void saveContactUsMessage(ContactUsMessage message) throws ServiceException {
	message.setCreationDate(Calendar.getInstance().getTimeInMillis());
	contactUsMessageRepository.save(message);
	logger.info("Hey!!! Someone just contact us!!!!!! subject: {0}" + message.getSubject());
	
	//	send a notification email
	EmailAddress from = new EmailAddress();
	from.setEmail(contactUsFrom);
	from.setName(contactUsfromName);
	EmailAddress to = new EmailAddress();
	to.setEmail(contactUsTo);
	StringBuffer body = new StringBuffer();
	Calendar calendar = Calendar.getInstance();
	calendar.setTimeInMillis(message.getCreationDate());
	body.append(" Date: " + DateFormat.getDateTimeInstance().format(calendar.getTime()));
	body.append("\n");
	body.append(" Contact Us From : " + message.getEmail());
	body.append("\n");
	body.append(" Subject : " + message.getSubject());
	body.append("\n");
	body.append(" Message: " + message.getMessage());
	
	notificationService.sendEmail(from, to, "Email Got Social Contact: " + message.getSubject(), body.toString());
    }

    /* (non-Javadoc)
     * @see blindcode.springmvc.service.PublicService#isValidCaptchaResponse(java.lang.String)
     */
    @Override
    public boolean isValidCaptchaResponse(String response) throws ServiceException {
	List<NameValuePair> nvps = new ArrayList<NameValuePair>();
	nvps.add(new BasicNameValuePair("secret", captchaSecret));
	nvps.add(new BasicNameValuePair("response", response));
	
	try {
	    String verification = post(captchaVerifyUrl, nvps);
	    ReCaptchaResponse verificationResponse = gson.fromJson(verification, ReCaptchaResponse.class);
	    if (logger.isDebugEnabled() &&
		    !verificationResponse.success){
		logger.debug("Captcha validation error: " + verification);
	    }
	    return verificationResponse.success;
	} catch (IOException e) {
	    logger.error("Error validating captcha", e);
	    throw new ServiceException(e, "captcha.error");
	}
    }
    
    public String post(String url, List<NameValuePair> nvps) throws ClientProtocolException, IOException{
	CloseableHttpClient httpclient = HttpClients.createDefault();
	
	HttpPost httpPost = new HttpPost(url);
	httpPost.setEntity(new UrlEncodedFormEntity(nvps));
	CloseableHttpResponse response = httpclient.execute(httpPost);

	String result = null;
	try {
	    if (logger.isDebugEnabled()){
		logger.debug(response.getStatusLine().toString());
	    }
	    HttpEntity entity = response.getEntity();
	    StringBuffer buffer = new StringBuffer();
	    BufferedReader br = new BufferedReader(new InputStreamReader(entity.getContent()));
	    String line;
	    while ((line = br.readLine())!= null) {
		buffer.append(line);
	    }
	    result = buffer.toString();
	} finally {
	    response.close();
	}
	return result;
    }
    
    protected class ReCaptchaResponse{
	protected boolean success;
	protected String[] errorCodes;
    }
}
