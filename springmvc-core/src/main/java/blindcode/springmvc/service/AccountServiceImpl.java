/**
 * 
 */
package blindcode.springmvc.service;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.apache.commons.lang3.StringUtils;
import org.contextio.clientapi.ContextIOService;
import org.contextio.common.AccountSourceDiscover;
import org.contextio.common.EmailFolder;
import org.contextio.common.EmailMessage;
import org.contextio.common.EmailMessageFilter;
import org.contextio.common.EmailMessageFullBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import blindcode.springmvc.common.Account;
import blindcode.springmvc.common.AccountSalt;
import blindcode.springmvc.common.AccountSource;
import blindcode.springmvc.common.AppRole;
import blindcode.springmvc.common.EmailCombined;
import blindcode.springmvc.common.EmailShared;
import blindcode.springmvc.common.SearchResult;
import blindcode.springmvc.common.SignupCode;

/**
 * Service to expose CRUD methods of {@link Account} document
 * 
 * @author David
 *
 */
@Service
public class AccountServiceImpl implements AccountService {

    private static Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountSaltRepository accountSaltRepository;

    @Autowired
    private SignupCodeRepository signupCodeRepository;

    @Autowired
    private ContextIOService contextIOService;

    @Autowired
    private SharingService sharingService;

    private Object creationLock = new Object();

    private static SecureRandom secureRandom;
    private static SecretKeyFactory secretKeyFactory;

    private EmailFolderComparator emailFolderComparator = new EmailFolderComparator();

    static {
	try {
	    // Generating password salt for encryption
	    secureRandom = SecureRandom.getInstance("SHA1PRNG");

	    secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
	} catch (NoSuchAlgorithmException e) {
	    // This should never happen unless java change its implementation...
	    logger.error("Error generating Secure Hash with algorithm: SHA1PRNG, PBKDF2WithHmacSHA1", e);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * blindcode.springmvc.service.AccountService#create(blindcode.springmvc
     * .common.Account)
     */
    @Override
    public Account create(Account account) throws ServiceException {
	logger.debug("Creating account");
	// TODO DGD This lock is inefficiency but no time right now. Change it
	// for a lock related with the email field. Also tune the find by email
	// method
	synchronized (creationLock) {
	    if (accountRepository.findByEmail(account.getEmail()) != null) {
		throw new ServiceException("error.account.exists");
	    }
	    // check invitation code
	    if (!isSigupCodeValid(account)){
		logger.error("Signup code provided not valid:" + account.getSignupCode());
		throw new ServiceException("error.account.sigupcode.invalid");		
	    }

	    // First save the account in contextio
	    String remoteId = null;
	    try {
		remoteId = contextIOService.addAccount(toContextIOAccount(account));
	    } catch (Exception e) {
		logger.error("Exception thrown trying to add account to contextio", e);
		throw new ServiceException("error.account.create");
	    }
	    account.setRemoteId(remoteId);
	    byte[] passwordSalt = getNextSalt();
	    try {
		account.setPassword(new String(getEncryptedPassword(account.getPassword(), passwordSalt)));
	    } catch (InvalidKeySpecException e) {
		// This should never happen
		logger.error("Exception thrown trying to generate encrypted password", e);
		throw new ServiceException("error.account.create");
	    }
	    // set account user role
	    if (account.getRoles() == null) {
		account.setRoles(new LinkedHashSet<AppRole>());
	    }
	    account.getRoles().add(AppRole.ROLE_USER);
	    Account createdAccount = accountRepository.save(account);
	    AccountSalt accountSalt = new AccountSalt(createdAccount.getId(), passwordSalt);
	    accountSaltRepository.save(accountSalt);
	    return createdAccount;
	}
    }

    private boolean isSigupCodeValid(Account account) {
	if (StringUtils.isBlank(account.getSignupCode())){
	    return false;
	}
	SignupCode code = signupCodeRepository.findByCode(account.getSignupCode().toUpperCase());
	return code != null && code.isActive(); 
    }

    /**
     * 
     * @param password
     *            to encrypt
     * @return password encrypted
     * @throws InvalidKeySpecException
     */
    private byte[] getEncryptedPassword(String password, byte[] salt) throws InvalidKeySpecException {
	PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 1000, 64 * 8);
	byte[] result;
	try {
	    result = secretKeyFactory.generateSecret(spec).getEncoded();
	} catch (InvalidKeySpecException e) {
	    throw e;
	} finally {
	    spec.clearPassword();
	}
	return result;
    }

    /**
     * 
     * @param account
     *            to convert
     * @return converts from {@link Account} to
     *         {@link org.contextio.common.Account}
     */
    private org.contextio.common.Account toContextIOAccount(Account account) {
	if (account == null) {
	    return null;
	}
	org.contextio.common.Account result = new org.contextio.common.Account();
	result.setEmail(account.getEmail());
	result.setFirstName(account.getFullName());
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * blindcode.springmvc.service.AccountService#save(blindcode.springmvc.common
     * .Account)
     */
    @Override
    public Account save(Account account) {
	logger.debug("Updating account");
	return accountRepository.save(account);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * blindcode.springmvc.service.AccountService#findByEmail(java.lang.String)
     */
    @Override
    public Account findByEmail(String email) {
	return accountRepository.findByEmail(email);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * blindcode.springmvc.service.AccountService#chanllengePassword(blindcode
     * .springmvc.common.Account, java.lang.String)
     */
    @Override
    public boolean chanllengePassword(Account account, String unencryptedPassword) throws ServiceException {
	Assert.notNull(account);
	if (StringUtils.isBlank(unencryptedPassword)) {
	    return false;
	}
	AccountSalt accountSalt = accountSaltRepository.findOne(account.getId());
	if (accountSalt == null) {
	    logger.error(MessageFormat.format("Unable to find salt for account accountId/accountEmail:  {0} / {1}, : ", account.getId(), account.getEmail()));
	    throw new ServiceException("error.account.challengePassword");
	}
	try {
	    String challengePassword = new String(getEncryptedPassword(unencryptedPassword, accountSalt.getSalt()));
	    return challengePassword.equals(account.getPassword());
	} catch (InvalidKeySpecException e) {
	    // this should never happens
	    logger.error("Exception thrown trying to generate encrypted password", e);
	    return false;
	}
    }

    /**
     * 
     * @return the randomized password salt
     */
    private byte[] getNextSalt() {
	byte[] passwordSalt = new byte[16];
	secureRandom.nextBytes(passwordSalt);
	return passwordSalt;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * blindcode.springmvc.service.AccountService#addAccountSource(java.lang
     * .String, blindcode.springmvc.common.AccountSource)
     */
    @Override
    public void addAccountSource(String accountId, AccountSource accountSource) throws ServiceException {
	try {
	    contextIOService.addAccountSource(accountId, toAccoutSourceContext(accountSource));
	} catch (Exception e) {
	    logger.error("An exception thrown calling external API contextIOService.addAccountSource for account: " + accountId, e);
	    throw new ServiceException("error.account.addsource");
	}
    }

    /**
     * @param accountSource
     *            to convert
     * @return the {@link org.contextio.common.AccountSource} from
     *         {@link AccountSource}
     */
    private org.contextio.common.AccountSource toAccoutSourceContext(AccountSource accountSource) {
	org.contextio.common.AccountSource result = new org.contextio.common.AccountSource();
	result.setEmail(accountSource.getEmail());
	result.setServer(accountSource.getServer());
	result.setUsername(accountSource.getUsername());
	result.setUseSsl(accountSource.isUseSsl());
	result.setPort(accountSource.getPort());
	result.setPassword(accountSource.getPassword());
	return result;
    }

    /**
     * @param accountSource
     *            to convert
     * @return the {@link org.contextio.common.AccountSource} from
     *         {@link AccountSource}
     */
    private AccountSource toAccoutSource(org.contextio.common.AccountSource accountSource) {
	AccountSource result = new AccountSource();
	result.setEmail(accountSource.getEmail());
	result.setServer(accountSource.getServer());
	result.setUsername(accountSource.getUsername());
	result.setUseSsl(accountSource.isUseSsl());
	result.setPort(accountSource.getPort());
	result.setPassword(accountSource.getPassword());
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * blindcode.springmvc.service.AccountService#getAccountSources(java.lang
     * .String)
     */
    @Override
    public List<AccountSource> getAccountSources(String accountId) throws ServiceException {
	List<AccountSource> result = new ArrayList<AccountSource>();
	try {
	    List<org.contextio.common.AccountSource> sources = contextIOService.getAccountSources(accountId, null);
	    for (org.contextio.common.AccountSource source : sources) {
		result.add(toAccoutSource(source));
	    }
	} catch (Exception e) {
	    logger.error("Error retreiving account sources from account: " + accountId, e);
	    throw new ServiceException(e, "error.account.sources");
	}
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * blindcode.springmvc.service.AccountService#getEmailMessages(java.lang
     * .String, java.util.Map)
     */
    @Override
    public List<EmailCombined> getEmailMessages(String accountId, Map<EmailMessageFilter, String> filterParams) throws ServiceException {
	List<EmailCombined> result = new ArrayList<EmailCombined>();
	try {
	    List<EmailMessage> messages = contextIOService.getMessages(accountId, filterParams);
	    result = getEmailCombined(accountId, messages);
	} catch (Exception e) {
	    logger.error("Error retreiving messages from account: " + accountId, e);
	    throw new ServiceException(e, "error.account.messages");
	}
	return result;
    }

    private List<EmailCombined> getEmailCombined(String accountId, List<EmailMessage> messages) {
	List<EmailCombined> result = new ArrayList<EmailCombined>();
	for (EmailMessage message : messages) {
	    EmailCombined combined = new EmailCombined();

	    combined.setEmailMessage(message);

	    EmailShared sharedEmail = sharingService.getSharedEmailQuite(accountId, message.getMessageId());
	    if (sharedEmail == null) {
		sharedEmail = sharingService.createSeed(accountId, message);
	    }
	    combined.setEmailShared(sharedEmail);

	    result.add(combined);
	}
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * blindcode.springmvc.service.AccountService#searchEmailMessages(java.lang
     * .String, java.util.Map)
     */
    @Override
    public SearchResult<EmailCombined> searchEmailMessages(String accountId, Map<EmailMessageFilter, String> filterParams) throws ServiceException {
	SearchResult<EmailCombined> result = new SearchResult<EmailCombined>();
	// TODO DGD pagination is done in memory because a limitation in
	// ContextIO API
	try {
	    Integer limit = removeEmailFilter(filterParams, EmailMessageFilter.limit, 10);
	    Integer page = removeEmailFilter(filterParams, EmailMessageFilter.offset, 0);
	    List<EmailMessage> messagesNoPaging = contextIOService.getMessages(accountId, filterParams);

	    result.setLimit(limit);
	    result.setPage(page);
	    result.setTotal(messagesNoPaging.size());

	    int from = Math.max(0, page * limit);
	    int to = Math.min(messagesNoPaging.size(), (page + 1) * limit);

	    result.setResult(getEmailCombined(accountId, messagesNoPaging.subList(from, to)));
	} catch (Exception e) {
	    logger.error("Error retreiving messages from account: " + accountId, e);
	    throw new ServiceException(e, "error.account.messages");
	}
	return result;
    }

    private Integer removeEmailFilter(Map<EmailMessageFilter, String> filterParams, EmailMessageFilter key, Integer defaultValue) {
	String value = filterParams.remove(key);
	if (StringUtils.isBlank(value)) {
	    return defaultValue;
	}
	return Integer.valueOf(value);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * blindcode.springmvc.service.AccountService#getEmailFullBody(java.lang
     * .String, java.lang.String, java.lang.String)
     */
    @Override
    public EmailMessageFullBody getEmailFullBody(String accountId, String messageId, String type) throws ServiceException {
	try {
	    return contextIOService.getEmailFullBody(accountId, messageId, type);
	} catch (Exception e) {
	    logger.error("Error retreiving message body from account: " + accountId + " message id: " + messageId, e);
	    throw new ServiceException(e, "error.account.sources");
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * blindcode.springmvc.service.AccountService#getEmailFolders(java.lang.
     * String, java.lang.String)
     */
    @Override
    public List<EmailFolder> getEmailFolders(String accountId, String accountSourceLabel) throws ServiceException {
	try {
	    List<EmailFolder> result = contextIOService.getFolders(accountId, accountSourceLabel, null);

	    Collections.sort(result, emailFolderComparator);
	    return result;
	} catch (Exception e) {
	    logger.error("Error retreiving folders from account: " + accountId + " account source: " + accountSourceLabel, e);
	    throw new ServiceException(e, "error.account.folders");
	}
    }

    /* (non-Javadoc)
     * @see blindcode.springmvc.service.AccountService#discoverAccountSourceSettings(java.lang.String)
     */
    @Override
    public AccountSourceDiscover discoverAccountSourceSettings(String email) throws ServiceException {
	try {
	    return contextIOService.discoverAccoutSource(email);
	} catch (Exception e) {
	    logger.error("Error discovering email server setting of: " + email, e);
	    throw new ServiceException(e, "error.account.discovery");
	}
    }

}
