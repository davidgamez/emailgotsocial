/**
 * 
 */
package blindcode.springmvc.service;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import blindcode.springmvc.common.Account;

/**
 * @author David
 *
 */
public interface AccountRepository extends CrudRepository<Account, String> {

	/**
	 * Return all accounts
	 */
	List<Account> findAll();
		
	/**
	 * @param account to save
	 */
	@SuppressWarnings("unchecked")
	Account save(Account account);
	
	/**
	 * 
	 * @param email to match
	 * @return the account related to the <code>email</code> parameter
	 */
	Account findByEmail(String email);
	
}
