/**
 * 
 */
package blindcode.springmvc.service;

import org.springframework.data.repository.CrudRepository;

import blindcode.springmvc.common.EmailSharedContent;

/**
 * @author David
 *
 */
public interface EmailSharedRepository extends CrudRepository<EmailSharedContent, String>{

}
