/**
 * 
 */
package blindcode.springmvc.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedHashSet;
import java.util.Set;

import org.contextio.clientapi.ContextIOService;
import org.contextio.common.EmailMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import blindcode.springmvc.common.EmailShared;
import blindcode.springmvc.common.EmailSharedContent;
import blindcode.springmvc.common.ResourceShared;
import blindcode.springmvc.common.SeedResourceShared;
import blindcode.springmvc.common.SocialProvider;

/**
 * @author David
 *
 */
@Service
public class SharingServiceImpl implements SharingService {

    private static Logger logger = LoggerFactory.getLogger(SharingServiceImpl.class);

    @Autowired
    private EmailSharedRepository emailSharedRepository;

    @Autowired
    private ContextIOService contextIOService;

    /*
     * (non-Javadoc)
     * 
     * @see
     * blindcode.springmvc.service.SharingService#shareEmail(org.contextio.common
     * .EmailMessage)
     */
    @Override
    public EmailShared saveShareEmail(String accountId, EmailMessage email, SocialProvider[] socialProviders) throws ServiceException {
	EmailSharedContent result = emailSharedRepository.findOne(email.getMessageId());
	if (result == null) {
	    result = new EmailSharedContent();
	    transformToResourceShared(accountId, email, result);
	    result.setActive(true);
	    result.setCc(email.getAddresses().getCc());
	    result.setDate(email.getDate());
	    result.setFrom(email.getAddresses().getFrom());
	    result.setGmailMessageId(email.getGmailMessageId());
	    result.setGmailThreadId(email.getGmailThreadId());
	    result.setRemoteId(email.getMessageId());
	    result.setSharedDate(Calendar.getInstance().getTimeInMillis());
	    result.setSocialProviders(socialProviders);
	    result.setSubject(email.getSubject());
	    result.setTo(email.getAddresses().getTo());
	}

	SocialProvider[] providers = null;
	if (result.getSocialProviders() != null){
	    providers = result.getSocialProviders();
	}else{
	    providers = new SocialProvider[]{};
	}
	Set<SocialProvider> providersSet = new LinkedHashSet<SocialProvider>(Arrays.asList(providers));
	providersSet.addAll(Arrays.asList(socialProviders));
	result.setSocialProviders(providersSet.toArray(new SocialProvider[providersSet.size()]));
	
	try {
	    result.setSharedContent(contextIOService.getEmailFullBody(accountId, email.getMessageId(), "text/html").getContent());
	} catch (Exception e) {
	    logger.error("Error retreiving account sources from account: " + accountId, e);
	    throw new ServiceException(e, "error.account.sources");
	}

	emailSharedRepository.save(result);
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * blindcode.springmvc.service.SharingService#createSeed(org.contextio.common
     * .EmailMessage)
     */
    @Override
    public SeedResourceShared createSeed(String accountId, EmailMessage email) {
	SeedResourceShared result = new SeedResourceShared();
	transformToResourceShared(accountId, email, result);
	return result;
    }

    private void transformToResourceShared(String accountId, EmailMessage email, ResourceShared resourceShared) {
	resourceShared.setAccountId(accountId);
	resourceShared.setId(email.getMessageId());
	resourceShared.setUrlPath(generateUrlPath(accountId, email));
    }

    /**
     * 
     * @param accountId
     *            account identifier
     * @param email
     * @return the url section corresponding to the <code>email</code> of the
     *         <code>account</code>
     */
    private String generateUrlPath(String accountId, EmailMessage email) {
	StringBuffer result = new StringBuffer();
	result.append(SharingService.URL_SHARE_BASE);
	result.append(accountId);
	result.append("/email/");
	result.append(email.getMessageId());

	String urlSubject = null;
	if (email.getSubject() != null) {
	    urlSubject = email.getSubject().trim();
	    urlSubject = urlSubject.length() > 20 ? urlSubject.substring(0, 20) : urlSubject;
	} else {
	    urlSubject = "Email date: " + email.getDate();
	}
	try {
	    urlSubject = URLEncoder.encode(urlSubject, "UTF-8");
	} catch (UnsupportedEncodingException e) {
	    logger.error("This should not happen, error encoding subject: " + urlSubject, e);
	    urlSubject = "Email date: " + email.getDate();
	}
	// TODO DGD removed the subject until the encoding issue is fixed
	// result.append("/");
	// result.append(urlSubject);

	return result.toString();
    }

    @Override
    public EmailSharedContent getSharedEmail(String accountId, String messageId) throws ServiceException {
	EmailSharedContent result = emailSharedRepository.findOne(messageId);
	if (result == null) {
	    throw new ServiceException("sharedEmail.notFound");
	}
	return result;
    }

    @Override
    public EmailSharedContent getSharedEmailQuite(String accountId, String messageId) {
	return emailSharedRepository.findOne(messageId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * blindcode.springmvc.service.SharingService#saveShareEmail(java.lang.String
     * , java.lang.String, blindcode.springmvc.common.SocialProvider[])
     */
    @Override
    public EmailShared saveShareEmail(String accountId, String emailId, SocialProvider[] socialProviders) throws ServiceException {
	EmailMessage email = null;
	try {
	    email = contextIOService.getMessage(accountId, emailId, null);
	} catch (Exception e) {
	    logger.error("Error retreiving email: " + emailId);
	    throw new ServiceException(e, "sharing.message");
	}
	if (email == null) {
	    logger.error("Error retreiving email, not found message with id: " + emailId);
	    throw new ServiceException("sharing.message.notfound");
	}

	return saveShareEmail(accountId, email, socialProviders);
    }
}
