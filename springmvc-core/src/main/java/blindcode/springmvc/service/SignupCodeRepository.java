/**
 * 
 */
package blindcode.springmvc.service;

import org.springframework.data.repository.CrudRepository;

import blindcode.springmvc.common.SignupCode;

/**
 * @author David
 *
 */
public interface SignupCodeRepository extends CrudRepository<SignupCode, String>{

    /**
     * 
     * @param code to look for
     * @return the {@link SignupCode} instance associated to <code>code</code>
     */
    SignupCode findByCode(String code);
}
