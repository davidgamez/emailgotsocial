/**
 * 
 */
package blindcode.springmvc.service;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.contextio.common.EmailFolder;

/**
 * Compares two folders by a static defined names
 * @author David
 *
 */
public class EmailFolderComparator implements Comparator<EmailFolder> {

    public static final Map<String, Integer> orderMap = new HashMap<String, Integer>();
    
    static {
	orderMap.put("inbox", 0);
	orderMap.put("sent", 1);
	orderMap.put("trash", 2);
	orderMap.put("draft", 3);
    }
    
    @Override
    public int compare(EmailFolder folder1, EmailFolder folder2) {
	return getFolderOrder(folder1).compareTo(getFolderOrder(folder2));
    }

    private Integer getFolderOrder(EmailFolder folder) {
	if (folder == null ||
		StringUtils.isBlank(folder.getName())){
	    return Integer.MAX_VALUE;
	}
	Integer order = orderMap.get(folder.getName().trim().toLowerCase());
	if (order == null){
	    return Integer.MAX_VALUE;
	}
	return order;
    }

}
