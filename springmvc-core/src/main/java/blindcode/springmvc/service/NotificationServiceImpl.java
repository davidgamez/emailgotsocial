/**
 * 
 */
package blindcode.springmvc.service;

import org.apache.commons.lang3.StringUtils;
import org.contextio.common.EmailAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sendgrid.SendGrid;
import com.sendgrid.SendGrid.Email;

/**
 * @author David
 *
 */
@Service
public class NotificationServiceImpl implements NotificationService {

    private static Logger logger = LoggerFactory.getLogger(NotificationServiceImpl.class);
    
    @Autowired
    SendGrid sendGrid;
    
    /* (non-Javadoc)
     * @see blindcode.springmvc.service.NotificationService#sendEmail(org.contextio.common.EmailAddress, org.contextio.common.EmailAddress, java.lang.String)
     */
    @Override
    public void sendEmail(EmailAddress from, EmailAddress to, String subject, String body) throws ServiceException {
	Email email = new Email();
	email.setFrom(from.getEmail());
	if (StringUtils.isNotBlank(from.getName())){
	    email.setFromName(from.getName());    
	}
	email.addTo(to.getEmail());
	if (StringUtils.isNotBlank(to.getName())){
	    email.addToName(to.getName());    
	}
	email.setSubject(subject);
	email.setText(body);

	try {
	    sendGrid.send(email);
	} catch (Exception e) {
	    logger.error("Error sending email", e);
	    throw new ServiceException(e, "notification.sendmail.failed");
	}

    }

}
