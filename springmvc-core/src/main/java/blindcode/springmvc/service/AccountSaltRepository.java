/**
 * 
 */
package blindcode.springmvc.service;

import org.springframework.data.repository.CrudRepository;

import blindcode.springmvc.common.AccountSalt;

/**
 * @author David
 *
 */
public interface AccountSaltRepository extends CrudRepository<AccountSalt, String> {

	/**
	 * 
	 * @param accountId account identifier
	 * @return the {@link AccountSalt} associated to <code>accountId</code>
	 */
	AccountSalt findByAccountId(String accountId);
}
