/**
 * 
 */
package blindcode.springmvc.service;

import org.contextio.common.EmailMessage;

import blindcode.springmvc.common.EmailShared;
import blindcode.springmvc.common.EmailSharedContent;
import blindcode.springmvc.common.SeedResourceShared;
import blindcode.springmvc.common.SocialProvider;

/**
 * Service interface defines the method to server sharing requests
 * 
 * @author David
 *
 */
public interface SharingService {
    
    String URL_SHARE_BASE = "/share/";
    
    /**
     * Save a share email 
     * 
     * @param accountId account identifier
     * @param email context IO email to share
     * @param socialProviders social providers that shares the email
     * @return the shared email instance created
     * @throws ServiceException 
     */
    EmailShared saveShareEmail(String accountId, EmailMessage email, SocialProvider[] socialProviders) throws ServiceException;
    
    /**
     * Save a share email 
     * 
     * @param accountId account identifier
     * @param emailId context IO email identifier to share
     * @param socialProviders social providers that shares the email
     * @return the shared email instance created
     * @throws ServiceException 
     */
    EmailShared saveShareEmail(String accountId, String emailId, SocialProvider[] socialProviders) throws ServiceException;
    
    /**
     * @param email context IO email to 
     * @param accountId identifier of the account
     * @return a seed from the email
     */
    SeedResourceShared createSeed(String accountId, EmailMessage email);

    /**
     * 
     * @param messageId email identifier
     * @param accountId identifier of the account
     * @return a seed from the email
     */
//    SeedResourceShared createSeed(String accountId, String messageId);
    
    /**
     * 
     * @param accountId account identifier
     * @param messageId message identifier
     * @return the email shared content related with <code>accountId</code> and id <code>messageId</code>
     * @throws ServiceException 
     */
    EmailSharedContent getSharedEmail(String accountId, String messageId) throws ServiceException;
    
    /**
     * This method will return the shared email if exist otherwise null
     * @param accountId account identifier
     * @param messageId message identifier
     * @return the email shared content related with <code>accountId</code> and id <code>messageId</code>
     */
    EmailSharedContent getSharedEmailQuite(String accountId, String messageId);

}
