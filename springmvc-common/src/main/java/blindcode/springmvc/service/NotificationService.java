/**
 * 
 */
package blindcode.springmvc.service;

import org.contextio.common.EmailAddress;

/**
 * Service used to send notifications
 * @author David
 *
 */
public interface NotificationService {

    /**
     * Send an email from and to the given addresses
     * @param from address
     * @param to address
     * @param body of the email
     * @throws ServiceException
     */
    void sendEmail(EmailAddress from, EmailAddress to, String subject, String body) throws ServiceException;
}
