/**
 * 
 */
package blindcode.springmvc.service;

import java.util.List;
import java.util.Map;

import org.contextio.common.AccountSourceDiscover;
import org.contextio.common.EmailFolder;
import org.contextio.common.EmailMessageFilter;
import org.contextio.common.EmailMessageFullBody;

import blindcode.springmvc.common.Account;
import blindcode.springmvc.common.AccountSource;
import blindcode.springmvc.common.EmailCombined;
import blindcode.springmvc.common.SearchResult;

/**
 * Service to expose CRUD methods of {@link Account} document 
 * 
 * @author David
 *
 */
public interface AccountService {

	/**
	 * Create an account
	 * @param accountn to create
	 * @throws ServiceException 
	 */
	Account create(Account account) throws ServiceException;
	
	/**
	 * Save an account
	 * @param accountn to save
	 * @return account created
	 */
	Account save(Account account);
	
	/**
	 * 
	 * @param email to match
	 * @return the account related to the <code>email</code> parameter
	 */
	Account findByEmail(String email);

	/**
	 * 
	 * @param account to challenge the password
	 * @param unencryptedPassword  unencrypted password
	 * @return true is password match, false otherwise
	 * @throws ServiceException 
	 */
	boolean chanllengePassword(Account account, String unencryptedPassword) throws ServiceException;
	
	
	/**
	 * Add a source to an account denoted by <code>accountId</code> 
	 * @param accountId account identifier
	 * @param accountSource account source to add
	 * @throws ServiceException 
	 */
	void addAccountSource(String accountId, AccountSource accountSource) throws ServiceException;
	
	/**
	 * 
	 * @param accountId account identifier
 	 * @return the list of account sources associated to the account denoted by <code>accountId</code>
	 * @throws ServiceException 
	 */
	List<AccountSource> getAccountSources(String accountId) throws ServiceException;
	
	/**
	 * @param accountId account identifier
	 * @param filterParams
	 * @return the list of email messages
	 * @throws ServiceException 
	 */
	List<EmailCombined> getEmailMessages(String accountId, Map<EmailMessageFilter, String> filterParams) throws ServiceException;
	
	/**
	 * 
	 * @param accountId account identifier
	 * @param messageId message identifier
	 * @return the full body of the message denoted by <code>messageId</code>
	 * @throws ServiceException 
	 */
	EmailMessageFullBody getEmailFullBody(String accountId, String messageId, String type) throws ServiceException;
	
	/**
	 * @param accountId account identifier
	 * @param filterParams
	 * @return SearchResult containing the list of email messages
	 * @throws ServiceException 
	 */
	SearchResult<EmailCombined> searchEmailMessages(String accountId, Map<EmailMessageFilter, String> filterParams) throws ServiceException;
	
	/**
	 * 
	 * @param accountId account identifier
	 * @param accountSourceLabel label of the account source if null the first account will be used
	 * @return the list of email folders associated with the account source 
	 * @throws ServiceException
	 */
	List<EmailFolder> getEmailFolders(String accountId, String accountSourceLabel) throws ServiceException;
	
	/**
	 * 
	 * @param email to discover IMAP settings
	 * @return {@link AccountSourceDiscover} associated to <code>email</code> if it's any
	 * @throws ServiceException 
	 */
	AccountSourceDiscover discoverAccountSourceSettings(String email) throws ServiceException;
}
