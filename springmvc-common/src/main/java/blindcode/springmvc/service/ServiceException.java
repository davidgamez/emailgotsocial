/**
 * 
 */
package blindcode.springmvc.service;

/**
 * Service related exception. It main purpose is to expose a localized exception
 * to be able escalate the message to the outer layer.
 * 
 * @author David
 *
 */
public class ServiceException extends Exception {

	private static final long serialVersionUID = -1541122818251973028L;

	private String message;

	/**
	 * 
	 * @param throwable associated exception
	 * @param message
	 */
	public ServiceException(String message) {
		super();
		this.message = message;
	}

	/**
	 * 
	 * @param throwable associated exception
	 * @param message
	 */
	public ServiceException(Throwable throwable, String message) {
		super(throwable);
		this.message = message;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	
}
