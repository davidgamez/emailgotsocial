/**
 * 
 */
package blindcode.springmvc.service;

import blindcode.springmvc.common.ContactUsMessage;

/**
 * Service interface defines the method to server sharing requests
 * 
 * @author David
 *
 */
public interface PublicService {
    
   
    /**
     * Save {@link ContactUsMessage} instance 
     * @param message to persist
     * @throws ServiceException
     */
    void saveContactUsMessage(ContactUsMessage message) throws ServiceException;

    /**
     * 
     * @param response captcha response
     * @return true is <code>response</code> is a valid captcha response
     * @throws ServiceException 
     */
    boolean isValidCaptchaResponse(String response) throws ServiceException;
}
