/**
 * 
 */
package blindcode.springmvc.msg;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;

/**
 * Wrapper of a {@link Message} class that 
 * @author David
 *
 */
public class Messages {

	private Map<MessageLevel, Collection<Message>> messagesMap = new HashMap<MessageLevel, Collection<Message>>();
	
	/**
	 * 
	 * @param level of the messages
	 * @return an unmodifiable collection with the messages associated with <code>level</code>, an unmodifiable empty collection if there is no validations with the passed level 
	 */
	@SuppressWarnings("unchecked")
	public Collection<Message> getMessages(MessageLevel level){
		Collection<Message> result = messagesMap.get(level);
		if (result == null){
			result = CollectionUtils.EMPTY_COLLECTION;
		}
		result = CollectionUtils.unmodifiableCollection(result);
		return result;
	}
	
	/**
	 * Add a message
	 * @param message to add
	 */
	public void addMessage(Message message){
		if (message.getLevel() == null){
			message.setLevel(Message.DEFAULT_MSG_LEVEL);
		}
		Collection<Message> validations = messagesMap.get(message.getLevel());
		if (validations == null){
			validations = new ArrayList<Message>();
			messagesMap.put(message.getLevel(), validations);
		}
		validations.add(message);
	}
	
	/**
	 * 
	 * @return the validations associated with {@link MessageLevel#ERROR}
	 */
	public Collection<Message> getErrors(){
		return getMessages(MessageLevel.ERROR);
	}

	/**
	 * 
	 * @return the validations associated with {@link MessageLevel#WARNING}
	 */
	public Collection<Message> getWarnings(){
		return getMessages(MessageLevel.WARNING);
	}

	/**
	 * 
	 * @return the validations associated with {@link MessageLevel#INFO}
	 */
	public Collection<Message> getInfos(){
		return getMessages(MessageLevel.INFO);
	}
	
	/**
	 * 
	 * @return all validations results
	 */
	public Collection<Message> getMessages(){
		Collection<Message> result =  new ArrayList<Message>();
		for (MessageLevel level: MessageLevel.values()){
			result.addAll(getMessages(level));
		}
		return CollectionUtils.unmodifiableCollection(result);
	}
}
