/**
 * 
 */
package blindcode.springmvc.msg;

/**
 * Enumerator to describe the message level
 *  
 * @author David
 *
 */
public enum MessageLevel {

    ERROR,
    
    WARNING,
    
    INFO
}
