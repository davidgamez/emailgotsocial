/**
 * 
 */
package blindcode.springmvc.msg;

import org.apache.commons.lang3.StringUtils;

/**
 * This class is the representation of a message
 * 
 * @author David
 *
 */
public class Message {

	public static final MessageLevel DEFAULT_MSG_LEVEL = MessageLevel.INFO;

	private String message;
	private Object[] messageArguments;
	private MessageLevel level;

	/**
	 * Default constructor
	 */
	public Message() {
		this.message = StringUtils.EMPTY;
		this.level = DEFAULT_MSG_LEVEL;
	}

	/**
	 * Create message instance with the default message level
	 * {@link Message#DEFAULT_MSG_LEVEL}
	 * 
	 * @param message
	 * @param messageArguments
	 */
	public Message(String message, Object... messageArguments) {
		this.message = message;
		this.messageArguments = messageArguments;
		this.level = DEFAULT_MSG_LEVEL;
	}

	/**
	 * @param message
	 * @param messageArguments
	 * @param level
	 */
	public Message(String message, MessageLevel level, Object[] messageArguments) {
		this.message = message;
		this.messageArguments = messageArguments;
		this.level = level;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the messageArguments
	 */
	public Object[] getMessageArguments() {
		return messageArguments;
	}

	/**
	 * @param messageArguments
	 *            the messageArguments to set
	 */
	public void setMessageArguments(Object[] messageArguments) {
		this.messageArguments = messageArguments;
	}

	/**
	 * @return the level
	 */
	public MessageLevel getLevel() {
		return level;
	}

	/**
	 * @param level
	 *            the level to set
	 */
	public void setLevel(MessageLevel level) {
		this.level = level;
	}
}
