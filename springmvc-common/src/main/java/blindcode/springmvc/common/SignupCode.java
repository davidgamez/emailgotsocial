/**
 * 
 */
package blindcode.springmvc.common;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author David
 *
 */
@Document(collection = "signup_code")
public class SignupCode {

    @Indexed(unique = true)
    private String code;
    private boolean active = false;
    
    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }
    
    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }
    
    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }
    
    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }
    
}
