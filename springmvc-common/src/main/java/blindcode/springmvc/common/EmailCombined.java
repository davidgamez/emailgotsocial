/**
 * 
 */
package blindcode.springmvc.common;

import org.contextio.common.EmailMessage;

/**
 * Contains the email message and other related attributes 
 * @author David
 *
 */
public class EmailCombined{

    /**
     * Email message from the original source
     */
    private EmailMessage emailMessage;
    
    /**
     * Shared email message with omitted information and could contain grayed out sections in the content  
     */
    private EmailShared emailShared;

    /**
     * @return the emailMessage
     */
    public EmailMessage getEmailMessage() {
        return emailMessage;
    }

    /**
     * @param emailMessage the emailMessage to set
     */
    public void setEmailMessage(EmailMessage emailMessage) {
        this.emailMessage = emailMessage;
    }

    /**
     * @return the emailShared
     */
    public EmailShared getEmailShared() {
        return emailShared;
    }

    /**
     * @param emailShared the emailShared to set
     */
    public void setEmailShared(EmailShared emailShared) {
        this.emailShared = emailShared;
    }
    
}
