/**
 * 
 */
package blindcode.springmvc.common;

/**
 * @author David
 *
 */
public class EmailSharedContent extends EmailShared{

    /**
     * The content from the email.
     */
    private String sharedContent;

    /**
     * @return the sharedContent
     */
    public String getSharedContent() {
        return sharedContent;
    }

    /**
     * @param sharedContent the sharedContent to set
     */
    public void setSharedContent(String sharedContent) {
        this.sharedContent = sharedContent;
    }
}
