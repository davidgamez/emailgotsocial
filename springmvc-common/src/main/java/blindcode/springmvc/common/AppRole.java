/**
 * 
 */
package blindcode.springmvc.common;

/**
 * Application roles
 * @author David
 *
 */
public enum AppRole {
	ROLE_USER,
	ROLE_ADMIN
}
