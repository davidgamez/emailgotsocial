/**
 * 
 */
package blindcode.springmvc.common;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author David
 *
 */
@Document(collection = "person")
public class Person {

	private String id;
	  private String name;
	  private int age;
	   
	  public Person(String name, int age) {
	    this.name = name;
	    this.age = age;
	  }
	  
	  public String getId() {
	    return id;
	  }
	  public String getName() {
	    return name;
	  }
	  public int getAge() {
	    return age;
	  }
}
