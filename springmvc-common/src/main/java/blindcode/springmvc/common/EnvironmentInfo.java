/**
 * 
 */
package blindcode.springmvc.common;

import java.util.Locale;


/**
 * @author David Gamez
 *
 */
public class EnvironmentInfo {

	private Locale locale;
	private String username;
	private Account account;
	
	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the account
	 */
	public Account getAccount() {
	    return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(Account account) {
	    this.account = account;
	}
	
}
