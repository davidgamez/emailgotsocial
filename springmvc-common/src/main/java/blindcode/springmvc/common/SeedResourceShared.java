/**
 * 
 */
package blindcode.springmvc.common;

/**
 * This class is intended to be used in the case the resource is not shared yet but we need the base information before hand to provide it to the social service provider
 * @author David
 *
 */
public class SeedResourceShared extends EmailShared {

    /**
     * Default constructor
     */
    public SeedResourceShared(){
	super.setActive(false);
    }
    
    /* (non-Javadoc)
     * @see blindcode.springmvc.common.ResourceShared#setActive(boolean)
     */
    @Override
    public void setActive(boolean active){
	throw new UnsupportedOperationException("A Seed Resource Shared cannot be active");
    }
}
