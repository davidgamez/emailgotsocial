/**
 * 
 */
package blindcode.springmvc.common;

/**
 * Social service provider ex. facebook, google_plus, etc
 * @author David
 *
 */
public enum SocialProvider {

    FACEBOOK
}
