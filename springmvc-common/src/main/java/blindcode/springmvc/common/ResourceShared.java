/**
 * 
 */
package blindcode.springmvc.common;

/**
 * Base generic sahred resource
 * @author David
 *
 */
public abstract class ResourceShared {

    /**
     * identifier
     */
    private String id;
    
    /**
     * Social service providers where the email was shared
     */
    private SocialProvider[] socialProviders;
    
    /**
     * True if the shared email is active false otherwise
     */
    private boolean active;
    
    /**
     * Portion of the URL path that represents the email
     */
    private String urlPath;
    
    /**
     * Account identifier
     */
    private String accountId;
    
    /**
     * Date were the resource was shared 
     */
    private long sharedDate;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the socialProviders
     */
    public SocialProvider[] getSocialProviders() {
        return socialProviders;
    }

    /**
     * @param socialProviders the socialProviders to set
     */
    public void setSocialProviders(SocialProvider[] socialProviders) {
        this.socialProviders = socialProviders;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @return the urlPath
     */
    public String getUrlPath() {
        return urlPath;
    }

    /**
     * @param urlPath the urlPath to set
     */
    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * @return the sharedDate
     */
    public long getSharedDate() {
        return sharedDate;
    }

    /**
     * @param sharedDate the sharedDate to set
     */
    public void setSharedDate(long sharedDate) {
        this.sharedDate = sharedDate;
    }
    
}
