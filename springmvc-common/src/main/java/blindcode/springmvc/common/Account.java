/**
 * 
 */
package blindcode.springmvc.common;

import java.util.Set;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * The representation of a registered account
 * 
 * @author David
 *
 */
@Document(collection = "account")
public class Account {

	@Id
	private String id;
	private String remoteId;
	@NotEmpty
	private String fullName;
	@NotEmpty
	@Email
	private String email;
	@NotEmpty
	private String password;
	private Set<AppRole> roles;
	private boolean active;
	private String signupCode; 
	
	public Account(){
//		default constructor, nothing to do here	
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName
	 *            the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the remoteId
	 */
	public String getRemoteId() {
		return remoteId;
	}

	/**
	 * @param remoteId the remoteId to set
	 */
	public void setRemoteId(String remoteId) {
		this.remoteId = remoteId;
	}

	/**
	 * @return the roles
	 */
	public Set<AppRole> getRoles() {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(Set<AppRole> roles) {
		this.roles = roles;
	}

	/**
	 * @return the signupCode
	 */
	public String getSignupCode() {
	    return signupCode;
	}

	/**
	 * @param signupCode the signupCode to set
	 */
	public void setSignupCode(String signupCode) {
	    this.signupCode = signupCode;
	}
}
