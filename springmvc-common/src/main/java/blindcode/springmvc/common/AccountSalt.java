/**
 * 
 */
package blindcode.springmvc.common;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Encryption salt use to encrypt information
 * @author David
 *
 */
@Document(collection = "account_salt")
public class AccountSalt {

	@Id
	private String accountId;
	private byte[] salt;

	/**
	 * Constructor to initialize the class. It's the only moment to set the accountId and the salt properties.
	 * @param accountId identifier of an account
	 * @param salt associated to the account
	 */
	public AccountSalt(String accountId, byte[] salt) {
		super();
		this.accountId = accountId;
		this.salt = salt;
	}
	
	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}
		
	/**
	 * @return the salt
	 */
	public byte[] getSalt() {
		return salt;
	}
	
}
