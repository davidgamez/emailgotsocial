/**
 * 
 */
package blindcode.springmvc.common;

import org.contextio.common.EmailAddress;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author David
 *
 */
@Document (collection = "email_shared")
public class EmailShared extends ResourceShared{
    
    /**
     * remote identifier
     */
    private String remoteId;
    
    /**
     * email source of the account
     */
    private String accountSourceEmail;
    
    /**
     * date: numberUnix timestamp of message date
     */
    private long date;

    /**
     * addresses: objectEmail addresses and names of sender and recipients
     */
    private EmailAddress from;
    private EmailAddress[] to;
    private EmailAddress[] cc;

    /**
     * gmail_message_id: stringMessage id assigned by Gmail (only present if
     * source is a Gmail account)
     */
    private String gmailMessageId;

    /**
     * gmail_thread_id: stringThread id assigned by Gmail (only present if
     * source is a Gmail account)
     */
    private String gmailThreadId;

    /**
     * subject: stringSubject of the message
     */
    private String subject;

    /**
     * @return the accountSourceEmail
     */
    public String getAccountSourceEmail() {
        return accountSourceEmail;
    }

    /**
     * @param accountSourceEmail the accountSourceEmail to set
     */
    public void setAccountSourceEmail(String accountSourceEmail) {
        this.accountSourceEmail = accountSourceEmail;
    }

    /**
     * @return the remoteId
     */
    public String getRemoteId() {
        return remoteId;
    }

    /**
     * @param remoteId the remoteId to set
     */
    public void setRemoteId(String remoteId) {
        this.remoteId = remoteId;
    }

    /**
     * @return the date
     */
    public long getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(long date) {
        this.date = date;
    }

    /**
     * @return the from
     */
    public EmailAddress getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(EmailAddress from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public EmailAddress[] getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(EmailAddress[] to) {
        this.to = to;
    }

    /**
     * @return the cc
     */
    public EmailAddress[] getCc() {
        return cc;
    }

    /**
     * @param cc the cc to set
     */
    public void setCc(EmailAddress[] cc) {
        this.cc = cc;
    }

    /**
     * @return the gmailMessageId
     */
    public String getGmailMessageId() {
        return gmailMessageId;
    }

    /**
     * @param gmailMessageId the gmailMessageId to set
     */
    public void setGmailMessageId(String gmailMessageId) {
        this.gmailMessageId = gmailMessageId;
    }

    /**
     * @return the gmailThreadId
     */
    public String getGmailThreadId() {
        return gmailThreadId;
    }

    /**
     * @param gmailThreadId the gmailThreadId to set
     */
    public void setGmailThreadId(String gmailThreadId) {
        this.gmailThreadId = gmailThreadId;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }
}
