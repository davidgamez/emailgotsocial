/**
 * 
 */
package blindcode.springmvc.common;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;


/**
 * Account source class. 
 * @author David
 *
 */
public class AccountSource{
    	
    	@NotBlank @Email
	private String email;
    	@NotBlank
	private String server;
    	@NotBlank
	private String username;
	private boolean useSsl;
	@NotBlank
	private String port;
	@NotBlank
	private String password;
	
	/**
	 * @return the email
	 */
	public String getEmail() {
	    return email;
	}
	
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
	    this.email = email;
	}
	
	/**
	 * @return the server
	 */
	public String getServer() {
	    return server;
	}
	
	/**
	 * @param server the server to set
	 */
	public void setServer(String server) {
	    this.server = server;
	}
	
	/**
	 * @return the username
	 */
	public String getUsername() {
	    return username;
	}
	
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
	    this.username = username;
	}
	
	/**
	 * @return the useSsl
	 */
	public boolean isUseSsl() {
	    return useSsl;
	}
	
	/**
	 * @param useSsl the useSsl to set
	 */
	public void setUseSsl(boolean useSsl) {
	    this.useSsl = useSsl;
	}
	
	/**
	 * @return the port
	 */
	public String getPort() {
	    return port;
	}
	
	/**
	 * @param port the port to set
	 */
	public void setPort(String port) {
	    this.port = port;
	}
	
	/**
	 * @return the password
	 */
	public String getPassword() {
	    return password;
	}
	
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
	    this.password = password;
	}

}
