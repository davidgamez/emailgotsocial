EmailGotSocial
=========

Inspiration
=========
Abraham Lincoln's letter to his son's teacher could have been lost if written nowadays. We need to preserve information for our future generations. From presidents to lovers, everyone should be remembered.

What it does
=========
The website allow users to connect with their email accounts and share emails through social networks.

Built With
=========
Java, Javascript, AngularJS, REST, Spring, MongoDB, Material, Bootstrap, Thymeleaf, Spring-security, Spring-data...
