/**
 * Author: Ernesto Navarro
 * Date: 07/07/15.
 */

(function () {
    'use strict';

    angular.module('privateArea', ['ngRoute', 'ngMaterial', 'ngMessages', 'ngResource', 'ui.bootstrap', 'ngSanitize', 'ngTable']);

    angular.module('privateArea').config(config);
    angular.module('privateArea').run(run);

    config.$inject = ['$routeProvider', '$locationProvider', '$mdThemingProvider'];

    run.$inject = ['$rootScope', '$location', '$mdDialog'];

    function config($routeProvider, $locationProvider, $mdThemingProvider) {
        // Configure a dark theme with primary foreground yellow
        $mdThemingProvider.theme('docs-dark', 'default');

        $routeProvider
            .when('/', {
                controller: 'MainController',
                templateUrl: _SERVER_PATH + 'app/html/main/main.view',
                controllerAs: 'vm'
            })
            .when('/contactus', {
                controller: 'ContactUsController',
                templateUrl: _SERVER_PATH + 'app/html/contactus/contactus.view',
                controllerAs: 'vm'
            })
            .when('/share', {
                controller: 'ShareController',
                templateUrl: _SERVER_PATH + 'app/html/routing.view',
                controllerAs: 'vm'
            })            
            .otherwise({redirectTo: '/'});
    }

    function run($rootScope, $location, $mdDialog) {
        $rootScope.setRoute = setRoute;
        $rootScope.confirmLogout = confirmLogout;

        function setRoute(route) {
            $location.path(route);
        }
        
        function confirmLogout(ev, logoutAddress) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                  .title('Logout Confirmation')
                  .content('You are about to logout. Click on Logout to continue or Cancel otherwise.')
                  .ariaLabel('Lucky day')
                  .ok('Logout')
                  .cancel('Cancel')
                  .targetEvent(ev);
            $mdDialog.show(confirm).then(function() {
            	window.location.href = logoutAddress;;
            }, function() {
              console.log('logout cancelled')
            });
          };
    }
})();


