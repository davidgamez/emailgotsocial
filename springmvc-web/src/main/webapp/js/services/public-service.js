/**
 * Author: David Gamez
 * Date: 08/30/15.
 */
(function () {
    'use strict';

    angular.module(_ModuleName).factory('PublicService', PublicService);

    PublicService.$inject = ['$http'];
    function PublicService($http) {
        var publicService = {};

        publicService.submitContactUsForm = submitContactUsForm;

        var url = '/rest/public';

        return publicService;

        function submitContactUsForm(contactUsForm, contactUsCallback) {
            return $http.post(url + '/contact_us', contactUsForm).then(
            		function success(data) {
            			if (data.data.success){
                			contactUsCallback(data.data);
            			}else{
            				contactUsCallback({success: false, data: data.data});
            			}
            		}, 
            		function error(data) {
                			contactUsCallback({success: false, data: data.data});
            		});        	
        }
    }
})();


