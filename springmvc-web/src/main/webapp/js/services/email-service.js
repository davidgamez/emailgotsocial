/**
 * Author: Ernesto Navarro
 * Date: 07/07/15.
 */
(function () {
    'use strict';

    angular.module(_ModuleName).factory('EmailService', EmailService);

    EmailService.$inject = ['$http'];
    function EmailService($http) {
        var emailService = {};
        
        emailService.getFolders = getFolders;
        emailService.getMessages = getMessages;
        emailService.getMessageBody = getMessageBody;
        emailService.discovery = discovery;
        emailService.activeFolder = '';
        
        var url = '/rest/account';

        return emailService;

        function discovery(email, discoveryCallback){
            return $http.get(url + '/discovery?email=' + email).then(
            		function success(data) {
            			var imapSettings = {};
            			if (data.data.success){
            				imapSettings = data.data.response;
            			}
//            			TODO DGD create an error call
            			discoveryCallback(imapSettings);
            		}, 
            		error);        	
        }
        
        function getFolders(accountSourceLabel, foldersCallback){
        	var params = "";
        	if (accountSourceLabel != undefined && accountSourceLabel != null){
        		params = '?accountSourceLabel=' + accountSourceLabel;
        	}
            return $http.get(url + '/folders' + params).then(
            		function success(data) {
            			var folders = [];
            			if (data.data.success){
            				folders = data.data.response;
            			}
//            			TODO DGD create an error call
            			foldersCallback(folders);
            		}, 
            		error);        	
        }
        
        function getMessages(params, messagesCallback) {
        	var page = 0;
        	if (params != null && params.page() != null){
        		page = params.page() - 1;
        	}
        	var limit = 3;
        	if (params != null && params.count() != null){
        		limit = params.count();
        	}
        	var urlParams = '/emails?page=' + page + '&limit=' + limit;
        	
//        	applying search
        	if (params != null && params.filter().search != null &&
        			params.filter().search != undefined){
        		urlParams += '&search=' + params.filter().search;
        	}
        	
        	if (emailService.activeFolder != undefined &&
        			emailService.activeFolder != null){
        		urlParams += '&folder=' + emailService.activeFolder;
        	}
            return $http.get(url + urlParams).then(
            		function success(data) {
            			var messages = [];
            			if (data.data.success){
            				messages = data.data.response;
            			}
//            			TODO DGD create an error call
            			messagesCallback(messages);
            		}, 
            		error);
        }

        function getMessageBody(messageId, messageBodyCallback){
        	return $http.get(url + '/emailbody?messageId=' + messageId).then(
            		function success(data) {
            			console.log(data);
            			var messageBody = [];
            			if (data.data.success){
            				messageBody = data.data.response;
            			}
//            			TODO DGD create an error call
            			messageBodyCallback(messageBody);
            		}, 
            		error);
        }
        
        function error(data) {
            return function () {
                return {success: false, data: data}
            }
        }

        function success(data) {
            return data;
        }
    }
})();


