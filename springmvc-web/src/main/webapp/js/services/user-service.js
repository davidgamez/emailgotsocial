/**
 * Author: Ernesto Navarro
 * Date: 07/07/15.
 */
(function () {
    'use strict';

    angular.module(_ModuleName).factory('UserService', UserService);

    UserService.$inject = ['$http'];
    function UserService($http) {
        var userService = {};

        userService.create = create;
        userService.remove = remove;
        userService.update = update;
        userService.getSources = getSources;
        userService.addSource = addSource;
        var url = '/rest/account';

        return userService;

        function create(account) {
            return $http.post(url, account).then(success, error);
        }

        function exist(email) {
            return $http.get(url + '/exist?' + email).then(success, error);
        }

        function remove(account) {
            return $http.delete(url, account).then(success, error);
        }

        function update(account) {
            return $http.put(url, account).then(success, error);
        }

        function getSources(sourcesCallback) {
            return $http.get(url + '/sources').then(
                function success(data) {
                    var sources = [];
                    if (data.data.success) {
                        sources = data.data.response;
                    }
//            			TODO DGD create an error call
                    sourcesCallback(sources);
                },
                error);
        }

        function addSource(accountSource, addSourceCallback) {
            return $http.post(url + '/source', accountSource).then(
                    function success(data) {
                        var sources = [];
                        if (data.data.success) {
                            sources = data.data.response;
                        }
//                			TODO DGD create an error call
                        addSourceCallback(true);
                    },
                    error);            
        }

        function error(data) {
            return function () {
                return {success: false, data: data}
            }
        }

        function success(data) {
            return data;
        }
    }
})();


