/**
 * Author: Ernesto Navarro
 * Date: 07/07/15.
 */
(function () {
    'use strict';

    angular.module(_ModuleName).factory('SharingService', SharingService);

    SharingService.$inject = ['$http'];
    function SharingService($http) {
        var sharingService = {};

        sharingService.shareUrl = shareUrl;
        
        var url = '/rest/account';

        return sharingService;

        function shareUrl(shareUrl, provider, sharingCallback) {

            return $http.get(url + '/share?url=' + shareUrl + "&provider=" + provider).then(
            		function success(data) {
            			var messages = [];
            			if (data.data.success){
            				messages = data.data.response;
            			}
//            			TODO DGD create an error call
            			sharingCallback(true);
            		}, 
            		error);
        }

        function error(data) {
            return function () {
                return {success: false, data: data}
            }
        }

        function success(data) {
            return data;
        }
    }
})();


