/**

 * Author: David Gamez
 * Date: 04/08/15.
 */
(function () {
	    
    angular.module(_ModuleName).controller('LandingRouterController', LandingRouterController);

    LandingRouterController.$inject = ['$location', '$scope', '$timeout', '$rootScope'];

    function LandingRouterController($location, $scope, $timeout, $rootScope) {
        var vm = this;
        vm.initRoute = initRoute;
        
        function initRoute(routeName){
        	$rootScope.setRoute(routeName);
        }
    }
    
})();