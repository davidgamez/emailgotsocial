/**

 * Author: David Gamez
 * Date: 04/08/15.
 */
(function () {
	    
    angular.module('privateArea').controller('EmailFolderController', EmailFolderController);

    EmailFolderController.$inject = ['$scope', '$location'];

    function EmailFolderController($scope, $location) {
        var vm = this;
        vm.isCollapsed = false;
        vm.folders = [];
        vm.defaultFolder = '';
        
        function getFolders(accountSourceLabel) {
            EmailService.getFolders(accountSourceLabel, foldersCallback);
        }

        function foldersCallback(emailFolders){
        	console.log("folders: " + emailFolders);
        	vm.folders = folders.result;
        	if (vm.folders != null && 
        			vm.folders.length > 0){
        		vm.defaultFolder = vm.folders[0].name;        		
        	}
        }
        
    }
    
})();