/**

 * Author: David Gamez
 * Date: 04/08/15.
 */
(function () {
	    
    angular.module('privateArea').controller('MainController', MainController);

    MainController.$inject = ['EmailService', 'SharingService', '$location', '$scope', '$timeout', '$sce', 'ngTableParams'];

    function MainController(EmailService, SharingService, $location, $scope, $timeout, $sce, ngTableParams) {
        var vm = this;
        vm.activeEmail = {
        		loaded: false,
        		from: '',
        		to: null,
        		emailBody: '',
        		subject: '',
        		date: '',
        		};
        vm.getMessages = getMessages;
        vm.showMessage = showMessage;
        vm.setBodyContent = setBodyContent;
        vm.loadingEmail = false;
        vm.folderClass = '';
        
//        share this subscribe 
        stLight.subscribe("click",sharedEmailEvent);

        function sharedEmailEvent(event, service, urlShared){
        	console.log("service: " + service);
        	var url = new URL(urlShared);
        	SharingService.shareUrl(url.pathname, service, sharedEmailCallback)
        }
        
        function sharedEmailCallback(sharedId){
        	console.log("url shared saved");
        }
        
        $scope.$on('switchFolder', function(event, args) {
        	var folder = {name: EmailService.activeFolder};
        	if (args.foldername != null &&
        			args.foldername != undefined){
        		folder = args.foldername;
        	}
        	$scope.vm.getMessages(folder);
        });
        
        
        function getMessages(folder) {
        	console.log('loading messages');
        	EmailService.activeFolder = folder.name;
        	$scope.messageTable.reload();
        }

        function messageCallback(messages){
        	vm.messages = messages.result;
//        	locate sharethis 
        	$timeout(stButtons.locateElements, 0);
        }

        function showMessage(emailMessage, urlPath) {
        	$scope.vm.activeEmail = {
        			loaded: true,
            		from: emailMessage.addresses.from.name,
            		emailBody: '',
            		to: emailMessage.addresses.to,
            		subject: emailMessage.subject,
            		date: new Date(emailMessage.date),
            		urlPath: urlPath
            };
        	vm.loadingEmail = true;
        	EmailService.getMessageBody(emailMessage.messageId, messageBodyCallback);
        }

        function messageBodyCallback(messageBody){
        	setBodyContent(messageBody.content);  
        	vm.loadingEmail = false;
        }
        
        function setBodyContent(content) {
            $scope.vm.activeEmail.emailbody = $sce.trustAsHtml(content);
        }

        function clearActiveEmail(){
        	$scope.vm.activeEmail = {
        			loaded: false,
            		from: '',
            		to: null,
            		emailBody: '',
            		subject: '',
            		date: '',
            };
        }
        
        vm.messages = [];
        
        $scope.$on('loadedFolders', function(event, args) {
        	
            $scope.messageTable = new ngTableParams({
                page: 1,            // show first page
                count: 10,           // count per page
                filter: {
                    search: ''       // initial filter
                }
            }, {
                total: 0, // length of data
                getData: function($defer, params) {
                    // ajax request to api
//                	params.filter = $scope.filter;
                	EmailService.getMessages(params, function(data) {
                        $timeout(function() {
                        	console.log("data loaded");
                            // update table params
                            params.total(data.total);
                            // set new data
                            $defer.resolve(data.result);
//                        	locate sharethis 
                        	$timeout(stButtons.locateElements, 0);
                        }, 500);
                    });
                }
            });
            
        });
    }
    
    angular.module('privateArea').
    directive('message', function () {
        return {
            templateUrl: _SERVER_PATH + 'app/html/main/message.view'
        };
    })
    .directive('messages', function () {
        return {
            templateUrl: _SERVER_PATH + 'app/html/main/messages.view'
        };
    })
    .directive('mainmenu', function () {
        return {
            templateUrl: _SERVER_PATH + 'app/html/main/mainmenu.view'
        };
    })
    .directive('contactus', function () {
        return {
            templateUrl: _SERVER_PATH + 'app/html/contactus/contactus.view'
        };
    })
    ;
    
})();