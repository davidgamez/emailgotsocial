/**
 *
 * Author: David Gamez Date: 04/08/15.
 */
(function () {

    angular.module('privateArea').controller('MainMenuController',
        MainMenuController);

    MainMenuController.$inject = ['UserService', 'EmailService', '$scope', '$mdDialog', '$rootScope'];

    function MainMenuController(UserService, EmailService, $scope, $mdDialog, $rootScope) {
        var vm = this;
        vm.folders = [];
        vm.defaultFolder = '';
        vm.sources = [];
        vm.getSources = getSources;
        vm.getFolders = getFolders;
        vm.getMessages = getMessages;
        vm.init = init;
        vm.openAccountSourceDialog = openAccountSourceDialog;

        $scope.status = {
            isFirstOpen: true,
            isFirstDisabled: false
        };

        function init() {
        	$rootScope.$on('accountSourceAdded', function () {
        		setTimeout(function(){
        				getSources();
        			}, 3000);
            });
            $scope.$on('loadedSources', function () {
                if (vm.sources.length > 0)
                    getFolders();
                else openAccountSourceDialog();
            });
            getSources();
        }

        function getSources() {
            UserService.getSources(sourcesCallback);
        }

        function sourcesCallback(sources) {
        	console.log(sources);
            $scope.vm.sources = sources;
            $scope.$emit('loadedSources', {});
        }

        function getFolders(accountSourceLabel) {
            EmailService.getFolders(accountSourceLabel, foldersCallback);
        }

        function foldersCallback(emailFolders) {
            console.log("folders: " + emailFolders);
            $scope.vm.folders = emailFolders;
            $scope.vm.defaultFolder = '';
            EmailService.activeFolder = '';
            if ($scope.vm.folders != null &&
                $scope.vm.folders.length > 0) {
                $scope.vm.defaultFolder = $scope.vm.folders[0].name;
                EmailService.activeFolder = $scope.vm.folders[0].name;
            }
            $scope.$emit('loadedFolders', {})
        }

        function openAccountSourceDialog(ev) {
            $mdDialog.show({
                controller: AccountSourceDialogController,
                templateUrl: 'app/html/main/account/accountsouces.dialog',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                controllerAs: 'vm'
            })
                .then(function (answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function () {
                    $scope.status = 'You cancelled the dialog.';
                });
        }

        function getMessages(folderName) {
            $scope.$emit('switchFolder', {foldername: folderName});
        }

    }

    angular.module('privateArea').controller('AccountSourceDialogController',
        AccountSourceDialogController);

    AccountSourceDialogController.$inject = ['UserService', 'EmailService', '$mdDialog', '$rootScope'];

    function AccountSourceDialogController(UserService, EmailService, $mdDialog, $rootScope) {
        var vm = this;
        vm.closeAccountSourceDialog = closeAccountSourceDialog;
        vm.saveAccountSource = saveAccountSource;
        vm.discovery = discovery;
        vm.editMode = false;
        vm.source = {};

        function saveAccountSource() {
            UserService.addSource(vm.source, saveAccountSourceCallback);
        }

        function saveAccountSourceCallback(success){
        	if (success){
        		$rootScope.$emit('accountSourceAdded', {});
        		vm.closeAccountSourceDialog();
        	}else{
        		alert("Error adding IMAP settings")
        	}
        }
        
        function closeAccountSourceDialog() {
            $mdDialog.cancel();
        }
        
        function discovery(){
        	if (vm.source.email != undefined &&
        			vm.source.email != null){
        		EmailService.discovery(vm.source.email, function(serverSettings){
        			if (serverSettings.found){
        				vm.source = {
        					email: serverSettings.email,
        					username: serverSettings.imap.username,
        					server: serverSettings.imap.server,
        					port: parseInt(serverSettings.imap.port),
        					useSsl: serverSettings.imap.useSsl
        				}
        			}else{
        				alert("Server information not available.");
//                	    $mdDialog.show(
//                      	      $mdDialog.alert()
//                      	        .parent(angular.element(document.querySelector('#popupContainer')))
//                      	        .clickOutsideToClose(true)
//                      	        .title('Error')
//                      	        .content('Server information not available.')
//                      	        .ariaLabel('Alert Dialog Demo')
//                      	        .ok('Close!')
//                      	    );
        			}
        		});
        	}else{
        		alert("Email required");
        	}
        }
    } 
    
})();
