/**
 * Author: Ernesto Navarro
 * Date: 07/07/15.
 */
(function () {
    angular.module('rolyPoly').controller('SignUpController', SignUpController);

    SignUpController.$inject = ['UserService', '$location', '$rootScope'];

    function SignUpController(UserService, $location, $rootScope) {
        var vm = this;
        vm.signup = signup;

        function signup() {
            var user = {
                fullName: vm.user.fullName,
                password: vm.user.password,
                email: vm.user.email,
                signupCode: vm.user.invitationcode
            };
            UserService.create(user).then(function (response) {
                if (response.data.success) {
                    $rootScope.setRoute("/login");
                    $rootScope.$apply();
                } else {
                    vm.error = response.data.messages[0].message;
                }
            });
        }
    }

    angular.module('rolyPoly').directive('email', function ($q, $timeout) {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {

                ctrl.$asyncValidators.email = function (modelValue, viewValue) {
                    /*

                     if (ctrl.$isEmpty(modelValue)) {
                     // consider empty model valid
                     return $q.when();
                     }
                     */

                    var def = $q.defer();

                    $timeout(function () {
                        /*  // Mock a delayed response
                         if (usernames.indexOf(modelValue) === -1) {
                         // The username is available
                         def.resolve();
                         } else {
                         def.reject();
                         }*/

                        def.reject

                    }, 1000);

                    return def.promise;
                };
            }
        }
    });

    angular.module('rolyPoly').directive('fieldtomatch', FieldsMatch);

    function FieldsMatch() {
        return {
            require: 'ngModel',
            link: watchandvalidation
        };

        function watchandvalidation(scope, elem, attrs, ctrl) {
            var firstField = attrs.fieldtomatch;
            var secondField = attrs.ngModel;
            scope.$watchGroup([firstField, secondField], function (value) {
                ctrl.$setValidity('mismatch', value[0] === value[1]);
            });
        }
    }
})();