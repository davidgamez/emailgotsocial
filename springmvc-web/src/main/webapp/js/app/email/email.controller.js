/**

 * Author: David Gamez
 * Date: 04/08/15.
 */
(function () {
	    
    angular.module('privateArea').controller('EmailController', EmailController);

    EmailController.$inject = ['EmailService', '$location', '$scope', '$timeout', '$sce'];

    function EmailController(EmailService, $location, $scope, $timeout, $sce) {
        var vm = this;
        vm.activeEmail = {
        		loaded: false,
        		from: '',
        		to: null,
        		emailBody: '',
        		subject: '',
        		date: '',
        		};
        vm.getMessages = getMessages;
        vm.showMessage = showMessage;
        vm.setBodyContent = setBodyContent;
        vm.loadingEmail = false;
        vm.folderClass = '';

        function showMessage(emailMessage) {
        	console.log(emailMessage.subject);
        	$scope.vm.activeEmail = {
        			loaded: true,
            		from: emailMessage.addresses.from.name,
            		emailBody: '',
            		to: emailMessage.addresses.to,
            		subject: emailMessage.subject,
            		date: new Date(emailMessage.date),
            };
        	vm.loadingEmail = true;
        	EmailService.getMessageBody(emailMessage.messageId, messageBodyCallback);
        }

        function messageBodyCallback(messageBody){
        	setBodyContent(messageBody.content);  
        	vm.loadingEmail = false;
        }
        
        function setBodyContent(content) {
            $scope.vm.activeEmail.emailbody = $sce.trustAsHtml(content);
        }

        function clearActiveEmail(){
        	$scope.vm.activeEmail = {
        			loaded: false,
            		from: '',
            		to: null,
            		emailBody: '',
            		subject: '',
            		date: '',
            };
        }
    }
    
})();