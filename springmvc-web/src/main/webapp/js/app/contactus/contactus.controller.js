/**
 * Author: Ernesto Navarro
 * Date: 07/07/15.
 */
(function () {
    angular.module(_ModuleName).controller('ContactUsController', ContactUsController);

    ContactUsController.$inject = ['PublicService', '$location', '$rootScope', '$scope', '$mdDialog', 'vcRecaptchaService'];

    function ContactUsController(PublicService, $location, $rootScope, $scope, $mdDialog, vcRecaptchaService) {
        var vm = this;
        vm.submitContactUsForm = submitContactUsForm;
        vm.fullName = "";
        vm.email = "";
        vm.subject = "";
        vm.message = "";
        
        $scope.model = {
                key: '6LddCgwTAAAAAHuoWvrgs1zBuzeJQG3M75GbOTiJ'
        };
        
        $scope.setResponse = function (response) {
            $scope.captchaResponse = response;
        };
        $scope.setWidgetId = function (widgetId) {
            $scope.widgetId = widgetId;
        };
        $scope.cbExpiration = function() {
            $scope.captchaResponse = null;
         };
         
        function submitContactUsForm() {
            /**
             * SERVER SIDE VALIDATION
             *
             * You need to implement your server side validation here.
             * Send the reCaptcha response to the server and use some of the server side APIs to validate it
             * See https://developers.google.com/recaptcha/docs/verify
             */
            console.log('sending the captcha response to the server', $scope.captchaResponse);
            var contactUsForm = {
            		contactUsMessage : {
                        fullName: vm.fullName,
                        email: vm.email,
                        subject: vm.subject,
                        message: vm.message            			
            		},
                    captchaResponse: $scope.captchaResponse 
                };
            PublicService.submitContactUsForm(contactUsForm, contactUsCallback);                            
        }
        
        function contactUsCallback(response){
            if (response.success) {
        	    $mdDialog.show(
        	      $mdDialog.alert()
        	        .parent(angular.element(document.querySelector('#popupContainer')))
          	        .clickOutsideToClose(true)
          	        .title('You reached us!')
          	        .content('Your message had reached us, we will back to you soon. Thanks, BlindCode Team.')
          	        .ariaLabel('Alert Dialog Demo')
          	        .ok('Got it!')
        	    );
            } else {
            	$mdDialog.show(
              	      $mdDialog.alert()
              	        .parent(angular.element(document.querySelector('#popupContainer')))
	        	        .clickOutsideToClose(true)
	        	        .title('Error')
	        	        .content('Error accurred processing your request. Please wait few minutes and try again. If the error persists contact us via twitter.')
	        	        .ariaLabel('Alert Dialog Demo')
	        	        .ok('Close')              	        
              	    );
            }	
            // In case of a failed validation you need to reload the captcha
            // because each response can be checked just once
            $scope.captchaResponse = null;
            vcRecaptchaService.reload($scope.widgetId);
        }
    }

    angular.module(_ModuleName).directive('email', function ($q, $timeout) {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {

                ctrl.$asyncValidators.email = function (modelValue, viewValue) {
                    /*

                     if (ctrl.$isEmpty(modelValue)) {
                     // consider empty model valid
                     return $q.when();
                     }
                     */

                    var def = $q.defer();

                    $timeout(function () {
                        /*  // Mock a delayed response
                         if (usernames.indexOf(modelValue) === -1) {
                         // The username is available
                         def.resolve();
                         } else {
                         def.reject();
                         }*/

                        def.reject

                    }, 1000);

                    return def.promise;
                };
            }
        }
    });
    
    angular.module(_ModuleName).directive('fieldtomatch', FieldsMatch);

    function FieldsMatch() {
        return {
            require: 'ngModel',
            link: watchandvalidation
        };

        function watchandvalidation(scope, elem, attrs, ctrl) {
            var firstField = attrs.fieldtomatch;
            var secondField = attrs.ngModel;
            scope.$watchGroup([firstField, secondField], function (value) {
                ctrl.$setValidity('mismatch', value[0] === value[1]);
            });
        }
    }
})();