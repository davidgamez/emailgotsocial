/**

 * Author: David Gamez
 * Date: 04/08/15.
 */
(function () {
	    
    angular.module(_ModuleName).controller('ShareController', ShareController);

    ShareController.$inject = ['$location', '$scope', '$routeParams', '$rootScope'];

    function ShareController($location, $scope, $routeParams, $rootScope) {
        var vm = this;
        $rootScope.routeParams = {account: '55a2d653ab31559d418b457e', message: '55a86f6d4d4c98ef2e8b456b'};
        $scope.templateUrl = '/share/email/' + $rootScope.routeParams.account + '/' + $rootScope.routeParams.message;
    }
    
    function initMessageRoute(routeParams){
    	$scope.templateUrl = '/share/email/' + routeParams.account + '/' + routeParams.message;
        console.log("messagesss" + $scope.templateUrl);    	
    }
})();