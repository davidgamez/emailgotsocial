/**
 * Author: Ernesto Navarro
 * Date: 07/07/15.
 */

(function () {
    'use strict';

    angular.module('rolyPoly', ['ngRoute', 'ngMaterial', 'ngMessages', 'ngResource', 'vcRecaptcha']);

    angular.module('rolyPoly').config(config);
    angular.module('rolyPoly').run(run);
    
    config.$inject = ['$routeProvider', '$locationProvider', '$mdThemingProvider'];

    run.$inject = ['$rootScope', '$location', '$routeParams', '$window'];

    function config($routeProvider, $locationProvider, $mdThemingProvider) {
        // Configure a dark theme with primary foreground yellow
        $mdThemingProvider.theme('docs-dark', 'default');

        $routeProvider
            .when('/', {
                controller: 'HomeController',
                templateUrl: _SERVER_PATH + 'app/html/home/home.view',
                controllerAs: 'vm'
            })
            .when('/signup', {
                controller: 'SignUpController',
                templateUrl: _SERVER_PATH + 'app/html/signup/signup.view',
                controllerAs: 'vm'
            })
            .when('/login', {
                controller: 'LogInController',
                templateUrl: _SERVER_PATH + 'app/html/login/login.view',
                controllerAs: 'vm'
            })
            .when('/contactus', {
                controller: 'ContactUsController',
                templateUrl: _SERVER_PATH + 'app/html/contactus/contactus.view',
                controllerAs: 'vm'
            })
            .when('/share', {
                controller: 'ShareController',
                templateUrl: _SERVER_PATH + 'app/html/routing.view',
                controllerAs: 'vm'
            })               
            .otherwise({redirectTo: '/'});
        
        // use the HTML5 History API
//        $locationProvider.html5Mode(true);
    }

    function run($rootScope, $location, $routeParams, $window) {
        $rootScope.setRoute = setRoute;

        if ($window.showMessageErrors != null &&
				$window.showMessageErrors != undefined){
        	$rootScope.showMessageErrors = $window.showMessageErrors; 
		}else{
			$rootScope.showMessageErrors = false;
		}
        
        function setRoute(route) {
        	$rootScope.containerClass = 'container';
        	if (route == 'share'){
        		$rootScope.containerClass = '';
        	}
        	
        	$rootScope.showMessageErrors = false;
        	$rootScope.$apply();
        	
            $location.path(route);
        }
        
    	$rootScope.$on('$routeChangeSuccess', function(e, current, pre) {
            console.log('Current route name: ' + $location.path());
            // Get all URL parameter
            console.log($routeParams);
    	});

    }
})();


