/**
 * This function will send a logout redirection
 * 
 * @param logoutAddress
 *            to redirect
 */
function logout(logoutAddress) {
	window.location.href = logoutAddress;
}