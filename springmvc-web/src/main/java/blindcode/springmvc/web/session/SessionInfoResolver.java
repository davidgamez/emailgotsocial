/**
 * 
 */
package blindcode.springmvc.web.session;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import blindcode.springmvc.web.common.SessionInfo;

/**
 * @author David Gamez
 * 
 */
@Component
public class SessionInfoResolver {

    	public static final String JS_APP_NAME_PUBLIC = "rolyPoly";
    	public static final String JS_APP_NAME_PRIVATE = "privateArea";
    	
	private static final String THEME_PARAMETER_NAME = "uiTheme";

	private static final Logger logger = LoggerFactory.getLogger(SessionInfoResolver.class);

	@Autowired
	private SessionInfo sessionInfo;

	@Autowired
	private MessageSource messageSource;

	@Value("${page.landing.unauthenticated}")
	private String unauthenticatedLandingPage;

	@Value("${page.landing.authenticated}")
	private String authenticatedLandingPage;

	@Value("#{'${theme.list}'.split(',')}")
	private List<String> themeList;

	@Value("${theme.default}")
	private String themeDefault;

	@Value("${defaultLanguage}")
	private String defaultLanguage;

	@Value("#{'${availableLanguages}'.split(',')}")
	private List<String> availableLanguages;

	@Value("${publicUrl}")
	private String publicUrl;
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
		sessionInfo.setLocale(RequestContextUtils.getLocale(request));
		// check if the theme was passed as a parameter
		String newTheme = request.getParameter(THEME_PARAMETER_NAME);
		if (StringUtils.isNotBlank(newTheme)) {
			if (themeList.contains(newTheme)) {
				logger.debug("Changing UI theme to: " + newTheme);
				sessionInfo.setUiTheme(newTheme);
			} else {
				logger.info("Theme will not be set because is not available: " + newTheme);
			}
		}
		if (sessionInfo.getUiTheme() == null) {
			logger.debug("Setting default theme: " + themeDefault);
			sessionInfo.setUiTheme(themeDefault);
		}
		Authentication auth = sessionInfo.getAuthentication(request);
		if (auth != null) {
			sessionInfo.setUsername(auth.getName());
		}
		sessionInfo.setJsAppName(auth != null? JS_APP_NAME_PRIVATE: JS_APP_NAME_PUBLIC);
		return true;
	}

	public void postHandle(HttpServletRequest request, ModelAndView modelAndView){
		// Languages
		modelAndView.addObject("availableLanguages", availableLanguages);
		String localeChangeLandingPage = sessionInfo.isAuthenticated(request) ? authenticatedLandingPage
				: unauthenticatedLandingPage;
		modelAndView.addObject("localeChangeLandingPage", localeChangeLandingPage);
		String currentLocale = sessionInfo.getLocale() != null ? sessionInfo.getLocale().toString() : defaultLanguage;
		modelAndView.addObject("currentLocale", currentLocale);
		modelAndView.addObject("publicUrl", publicUrl);
		modelAndView.addObject("globalJSDateFormat",
				messageSource.getMessage("date.format.js", null, sessionInfo.getLocale()));
		// if the version of the site only exits two languages
		if (availableLanguages.size() == 2) {
			modelAndView.addObject(
					"changeLocale",
					currentLocale.equals(availableLanguages.get(0)) ? availableLanguages.get(1) : availableLanguages
							.get(0));
		}
		// Themes
		modelAndView.addObject(THEME_PARAMETER_NAME, sessionInfo.getUiTheme() != null ? sessionInfo.getUiTheme() : themeDefault);
		modelAndView.addObject("uiAvailableTheme", themeList);

		modelAndView.addObject("sessionInfo", sessionInfo);
	}

}
