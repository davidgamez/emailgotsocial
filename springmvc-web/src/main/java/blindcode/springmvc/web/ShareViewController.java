/**
 * 
 */
package blindcode.springmvc.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import blindcode.springmvc.common.EmailSharedContent;
import blindcode.springmvc.service.SharingService;
import blindcode.springmvc.web.common.BaseController;

/**
 * Serve requests related with the sharing information. It land the user to the
 * requested shared resource page. The resource should be marked as shared
 * before requested via this controller. If it's not marked as shared this
 * controller will return invalid request status
 * 
 * @author David
 *
 */
@Controller
@RequestMapping(value = "/share")
public class ShareViewController extends BaseController {

    @Autowired
    SharingService sharingService;
    
    @RequestMapping(value = "{accountPublicId}/email/{shareMessageId}/{shareTitle}", method = RequestMethod.GET)
    public String get(@PathVariable String accountPublicId, @PathVariable String shareMessageId, @PathVariable String shareTitle, Model model) {
	return servePage(accountPublicId, shareMessageId, model);
    }
    
    @RequestMapping(value = "{accountPublicId}/email/{shareMessageId}", method = RequestMethod.GET)
    public String getNoTitle(@PathVariable String accountPublicId, @PathVariable String shareMessageId, Model model) {
	return servePage(accountPublicId, shareMessageId, model);
    }

    @RequestMapping(value = "email/{accountPublicId}/{shareMessageId}", method = RequestMethod.GET)
    public String getEmail(@PathVariable String accountPublicId, @PathVariable String shareMessageId, Model model) {
	EmailSharedContent email = sharingService.getSharedEmailQuite(accountPublicId, shareMessageId);
	model.addAttribute("email", email);
	return "app/share/share.view";
    }
    
    private String servePage(String accountPublicId, String shareMessageId, Model model) {
	EmailSharedContent email = sharingService.getSharedEmailQuite(accountPublicId, shareMessageId);
	model.addAttribute("email", email);
	return "email_viewer";
    }
}
