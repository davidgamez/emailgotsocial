package blindcode.springmvc.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import blindcode.springmvc.common.Account;
import blindcode.springmvc.msg.Message;
import blindcode.springmvc.msg.MessageLevel;
import blindcode.springmvc.web.common.BaseController;

/**
 * Sign up Controller
 *
 */
@Controller
public class LoginController extends BaseController{

	private static Map<Class<? extends AuthenticationException>, String> exceptionKeyMap = new HashMap<Class<? extends AuthenticationException>, String>();
	private static final String DEFAULT_ERROR_KEY = "error.auth.invalid";
	
	static{
		exceptionKeyMap.put(CredentialsExpiredException.class, "error.auth.account.credExpired");
		exceptionKeyMap.put(DisabledException.class, "error.auth.account.disabled");
		exceptionKeyMap.put(LockedException.class, "error.auth.account.locked");
		
	}
	
    @RequestMapping(value = "/loginerror*/**")
    public String handleLoginError(Model model, @Valid Account account, BindingResult binder, HttpServletRequest request, HttpServletResponse httpServletResponse){
        Object exception = request.getSession().getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
        if (exception == null){
        	return "redirect:/";
        }
        
        @SuppressWarnings("unchecked")
		List<Message> errors = (List<Message>) request.getAttribute("_messages_error");
        if (errors == null){
        	errors = new ArrayList<Message>();
        }
        
        Message message = new Message();
        message.setLevel(MessageLevel.ERROR);
        String errorKey = exceptionKeyMap.get(exception.getClass());
        if (errorKey == null){
        	errorKey = DEFAULT_ERROR_KEY;
        }
        message.setMessage(messageSource.getMessage(errorKey, null, sessionInfo.getLocale()));
		
		errors.add(message);
		model.addAttribute("_messages_error", errors);
    	return "index";
    }
}