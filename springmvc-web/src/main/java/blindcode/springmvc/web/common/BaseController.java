/**
 * 
 */
package blindcode.springmvc.web.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

/**
 * Base methods and fields for all controllers
 * @author David
 *
 */
public class BaseController {

    	/**
    	 * Class logger
    	 */
	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	protected MessageSource messageSource;
	
	/**
	 * Session information related with the HTTP session as the user details that is connected and so
	 */
	@Autowired
	protected SessionInfo sessionInfo;
	
}
