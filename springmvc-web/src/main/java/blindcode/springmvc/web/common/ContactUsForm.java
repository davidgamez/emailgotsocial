/**
 * 
 */
package blindcode.springmvc.web.common;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import blindcode.springmvc.common.ContactUsMessage;

/**
 * @author David
 *
 */
public class ContactUsForm {

    @NotNull
    private ContactUsMessage contactUsMessage;
    @NotEmpty
    private String captchaResponse;
    
    /**
     * @return the contactUsMessage
     */
    public ContactUsMessage getContactUsMessage() {
        return contactUsMessage;
    }
    
    /**
     * @param contactUsMessage the contactUsMessage to set
     */
    public void setContactUsMessage(ContactUsMessage contactUsMessage) {
        this.contactUsMessage = contactUsMessage;
    }
    
    /**
     * @return the captchaResponse
     */
    public String getCaptchaResponse() {
        return captchaResponse;
    }
    
    /**
     * @param captchaResponse the captchaResponse to set
     */
    public void setCaptchaResponse(String captchaResponse) {
        this.captchaResponse = captchaResponse;
    }
    
}
