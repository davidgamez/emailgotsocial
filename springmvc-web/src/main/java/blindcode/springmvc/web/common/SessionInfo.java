package blindcode.springmvc.web.common;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;

import blindcode.springmvc.common.Account;
import blindcode.springmvc.common.EnvironmentInfo;

public class SessionInfo extends EnvironmentInfo implements Serializable {

	private static final long serialVersionUID = -2583704181730487640L;
	
	private Account account;
	private String uiTheme;
	private Timestamp lastAccessDate;
	private String jsAppName;

	public boolean isAuthenticated(HttpServletRequest request) {
		Authentication authentication = getAuthentication(request);
		return authentication != null && !(authentication instanceof AnonymousAuthenticationToken)
				&& authentication.isAuthenticated();
	}

	public String getUiTheme() {
		return uiTheme;
	}

	public void setUiTheme(String uiTheme) {
		this.uiTheme = uiTheme;
	}

	public SecurityContext getSecurityContext(HttpServletRequest request) {
		HttpSession session = request.getSession(true);
		return (SecurityContext) session.getAttribute("SPRING_SECURITY_CONTEXT");
	}

	public Authentication getAuthentication(HttpServletRequest request) {
		SecurityContext securityContext = getSecurityContext(request);
		return securityContext != null ? securityContext.getAuthentication() : null;
	}

	/**
	 * @return the lastAccessDate
	 */
	public Timestamp getLastAccessDate() {
		return lastAccessDate;
	}

	/**
	 * @param lastAccessDate the lastAccessDate to set
	 */
	public void setLastAccessDate(Timestamp lastAccessDate) {
		this.lastAccessDate = lastAccessDate;
	}

	/**
	 * @return the account
	 */
	public Account getAccount() {
	    return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(Account account) {
	    this.account = account;
	}

	/**
	 * @return the jsAppName
	 */
	public String getJsAppName() {
	    return jsAppName;
	}

	/**
	 * @param jsAppName the jsAppName to set
	 */
	public void setJsAppName(String jsAppName) {
	    this.jsAppName = jsAppName;
	}
}
