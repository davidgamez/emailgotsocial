/**
 * 
 */
package blindcode.springmvc.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import blindcode.springmvc.web.session.SessionInfoResolver;

/**
 * @author David Gamez
 * 
 */
public class SessionInfoInterceptor extends HandlerInterceptorAdapter {
	
	@Autowired
	SessionInfoResolver sessionInfoResolver;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		return sessionInfoResolver.preHandle(request, response, handler);
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// The redirect expressions avoids the expanding of this sessions
		// attributes on the redirect URL
		if (modelAndView != null && !isRedirect(modelAndView)) {
			sessionInfoResolver.postHandle(request, modelAndView);
		}
	}

	private boolean isRedirect(ModelAndView modelAndView) {
		return StringUtils.isNotBlank(modelAndView.getViewName())
				&& modelAndView.getViewName().startsWith("redirect:");
	}

}
