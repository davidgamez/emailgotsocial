package blindcode.springmvc.web.rest;

import blindcode.springmvc.msg.Message;
import blindcode.springmvc.msg.MessageLevel;

/**
 * This class is the representation of an action message is designed to show messages in the UI
 * 
 * @author David
 *
 */
public class ActionMessage {

	public static final MessageLevel DEFAULT_MSG_LEVEL = MessageLevel.INFO;

	private String message;
	private String target;
	private MessageLevel level;

	/**
	 * Create message instance, if the level is null the default message level will be set
	 * {@link Message#DEFAULT_MSG_LEVEL}
	 * 
	 * @param message
	 * @param level
	 */
	public ActionMessage(String message, MessageLevel level) {
		this.message = message;
		this.target = null;
		this.level = level != null?level:DEFAULT_MSG_LEVEL;
	}

	
	/**
	 * Create message instance, if the level is null the default message level will be set
	 * {@link Message#DEFAULT_MSG_LEVEL}
	 * 
	 * @param message
	 * @param target
	 * @param level
	 */
	public ActionMessage(String message, String target, MessageLevel level) {
		this.message = message;
		this.target = target;
		this.level = level != null?level:DEFAULT_MSG_LEVEL;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the level
	 */
	public MessageLevel getLevel() {
		return level;
	}

	/**
	 * @return the target
	 */
	public String getTarget() {
		return target;
	}
}
