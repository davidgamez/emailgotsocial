/**
 * 
 */
package blindcode.springmvc.web.rest;

/**
 * Standard response related to request to controllers
 * 
 * @author David
 *
 */
public class ActionResponse {

	private boolean success;
	private ActionMessage[] messages;

	public ActionResponse(boolean success, ActionMessage[] messages) {
		super();
		this.success = success;
		this.messages = messages;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @return the messages
	 */
	public ActionMessage[] getMessages() {
		return messages;
	}

}
