/**
 * 
 */
package blindcode.springmvc.web.rest;

/**
 * @author David
 *
 */
public class ActionContentResponse<Response> extends ActionResponse {

	private Response response;
	
	public ActionContentResponse(boolean success, ActionMessage[] messages, Response response) {
		super(success, messages);
		this.response = response;
	}

	/**
	 * @return the response
	 */
	public Response getResponse() {
		return response;
	}

}
