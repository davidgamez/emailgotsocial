/**
 * 
 */
package blindcode.springmvc.web.rest;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import blindcode.springmvc.msg.MessageLevel;
import blindcode.springmvc.web.common.BaseController;

/**
 * Base behavior to all rest controllers
 * @author David
 *
 */
public class BaseRestController extends BaseController{

	/**
	 * Handle exception related to method arguments validation 
	 * 
	 * @param request
	 * @param response
	 * @param exception
	 * @return {@link ActionResponse} with details of the validation errors
	 */
	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ActionResponse handleRestValidationException(HttpServletRequest request, HttpServletResponse response, MethodArgumentNotValidException exception){
		ActionMessage[] messages = null;
		List<ObjectError> errors = exception.getBindingResult().getAllErrors();
		if (CollectionUtils.isNotEmpty(errors)){
//			Using a map with key {error_code}__{objectName}__{fieldName} to avoid duplicate error messages
			HashMap<String, ActionMessage> errorMap = new HashMap<String, ActionMessage>();
			ActionMessage actionMessage = null;
			for (ObjectError error: errors){
				String fieldName = "";
				if (error instanceof FieldError){
					fieldName = "." + ((FieldError)error).getField();
				}
				actionMessage = new ActionMessage(getLocalizedError(error), error.getObjectName() + fieldName, MessageLevel.ERROR);
				errorMap.put(error.getCode() + "__" + error.getObjectName() + "__" + fieldName, actionMessage);
			}
			messages = new ActionMessage[errorMap.size()];
			int i = 0;
			for (ActionMessage message: errorMap.values()){
				messages[i++] = message;
			}
		}
		return new ActionResponse(false, messages);
	}

	private String getLocalizedError(ObjectError error) {
		return messageSource.getMessage(error, sessionInfo.getLocale());
	}
}
