package blindcode.springmvc.web.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import blindcode.springmvc.service.PublicService;
import blindcode.springmvc.service.ServiceException;
import blindcode.springmvc.web.common.ContactUsForm;

/**
 * Sign up Controller
 *
 */
@RestController
@RequestMapping(value = "/rest/public")
public class PublicRestController extends BaseRestController {

    @Autowired
    private PublicService publicService;

    @RequestMapping(value = "/contact_us", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
    public ActionContentResponse<Boolean> post(@RequestBody @Valid ContactUsForm contactUsForm) throws ServiceException {
	if (publicService.isValidCaptchaResponse(contactUsForm.getCaptchaResponse())){
	    publicService.saveContactUsMessage(contactUsForm.getContactUsMessage());
	    return new ActionContentResponse<Boolean>(true, null, true);    
	}
	return new ActionContentResponse<Boolean>(false, null, false);
    }

}