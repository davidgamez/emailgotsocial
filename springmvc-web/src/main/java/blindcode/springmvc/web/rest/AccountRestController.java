package blindcode.springmvc.web.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.contextio.common.AccountSourceDiscover;
import org.contextio.common.EmailFolder;
import org.contextio.common.EmailMessageFilter;
import org.contextio.common.EmailMessageFullBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import blindcode.springmvc.common.Account;
import blindcode.springmvc.common.AccountSource;
import blindcode.springmvc.common.EmailCombined;
import blindcode.springmvc.common.EmailShared;
import blindcode.springmvc.common.SearchResult;
import blindcode.springmvc.common.SocialProvider;
import blindcode.springmvc.msg.MessageLevel;
import blindcode.springmvc.service.AccountService;
import blindcode.springmvc.service.ServiceException;
import blindcode.springmvc.service.SharingService;

/**
 * Sign up Controller
 *
 */
@RestController
@RequestMapping(value = "/rest/account")
public class AccountRestController extends BaseRestController {

    @Autowired
    private AccountService accountService;
    
    @Autowired
    SharingService sharingService;
    
    @RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
    public ActionResponse post(@RequestBody @Valid Account account) {
	account.setId(null);
	account.setActive(true);
	try {
	    accountService.create(account);
	} catch (ServiceException serviceException) {
	    logger.error("Exception signing up an account", serviceException);
	    ActionMessage message = new ActionMessage("Exception signing up the account", null, MessageLevel.ERROR);
	    return new ActionResponse(false, new ActionMessage[] { message });
	}
	ActionMessage message = new ActionMessage("Added Account", MessageLevel.INFO);
	return new ActionResponse(true, new ActionMessage[] { message });
    }

    /**
     * 
     * @param email
     *            to look for
     * @return response with true value if the email has an account associated,
     *         false otherwise
     */
    @RequestMapping(method = RequestMethod.GET, value = "exists")
    public ActionContentResponse<Boolean> exists(@RequestParam String email) {
	Account account = accountService.findByEmail(email);
	return new ActionContentResponse<Boolean>(true, null, account != null);
    }

    /**
     * 
     * @param accountSource account source to add
     * @return response with true value if the source was added successfully, false value and error description otherwise
     * @throws ServiceException
     */
    @RequestMapping(value = "source", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
    public ActionContentResponse<Boolean> addAccountSource(@RequestBody @Valid AccountSource accountSource) throws ServiceException {
	accountService.addAccountSource(sessionInfo.getAccount().getRemoteId(), accountSource);
	return new ActionContentResponse<Boolean>(true, null, true);
    }

    /**
     * 
     * @return response with the list of account sources related with the logged session account
     * @throws ServiceException 
     */
    @RequestMapping(value = "sources", method = RequestMethod.GET)
    public ActionContentResponse<List<AccountSource>> getAccountSources() throws ServiceException{
	List<AccountSource> sources = accountService.getAccountSources(sessionInfo.getAccount().getRemoteId());
	return new ActionContentResponse<List<AccountSource>>(true, null, sources);
    }
    
    /**
     * 
     * @return response with the list of account folders related with the logged session account and the source parameter
     * @throws ServiceException 
     */
    @RequestMapping(value = "folders", method = RequestMethod.GET)
    public ActionContentResponse<List<EmailFolder>> getFolders(@RequestParam(required = false) String accountSourceLabel) throws ServiceException{
	List<EmailFolder> sources = accountService.getEmailFolders(sessionInfo.getAccount().getRemoteId(), accountSourceLabel);
	return new ActionContentResponse<List<EmailFolder>>(true, null, sources);
    }
    
    /**
     * 
     * @param filterParams filter params for the email messages, null or empty are valid and means that all messages will be returned
     * @return the list of emails messages that match with the filter params criteria
     * @throws ServiceException
     */
    @RequestMapping(value = "emails", method = RequestMethod.GET)
    public ActionContentResponse<SearchResult<EmailCombined>> getEmailMessages(@RequestParam Map<String, String> filterParams) throws ServiceException{
	Map<EmailMessageFilter, String> emailMessageFilters = extractEmailMessageFilters(filterParams);
	
	SearchResult<EmailCombined> emailMessages = accountService.searchEmailMessages(sessionInfo.getAccount().getRemoteId(), emailMessageFilters);
	return new ActionContentResponse<SearchResult<EmailCombined>>(true, null, emailMessages);
    }
    
    private Map<EmailMessageFilter, String> extractEmailMessageFilters(Map<String, String> filterParams) {
//	TODO DGD improve this method
	Map<EmailMessageFilter, String> result = new HashMap<EmailMessageFilter, String>();
	if (filterParams != null){
	    
	    Integer page = filterParams.get("page") != null?Integer.valueOf(filterParams.get("page")):0;
	    Integer limit = filterParams.get("limit") != null?Integer.valueOf(filterParams.get("limit")):10;
	    String folder = StringUtils.isNotBlank(filterParams.get("folder")) ?filterParams.get("folder"):null;
	    String search = StringUtils.isNotBlank(filterParams.get("search")) ?filterParams.get("search"):null;
	    
	    result.put(EmailMessageFilter.offset, Integer.valueOf(page).toString());
	    result.put(EmailMessageFilter.limit, limit.toString());
	    if (folder != null){
		result.put(EmailMessageFilter.folder, folder);
	    }
	    if (search != null){
		result.put(EmailMessageFilter.subject, "/(" + search + ")/i");
	    }
	}
	return result;
    }

    @RequestMapping(value = "emailbody", method = RequestMethod.GET)
    public ActionContentResponse<EmailMessageFullBody> getEmailBody(@RequestParam String messageId) throws ServiceException{
//	TODO DGD review the message type
	EmailMessageFullBody emailBody = accountService.getEmailFullBody(sessionInfo.getAccount().getRemoteId(), messageId, "text/html");
	return new ActionContentResponse<EmailMessageFullBody>(true, null, emailBody);
    }
    
    @RequestMapping(value = "share", method = RequestMethod.GET)
    public ActionContentResponse<String> share(@RequestParam String url, String provider) throws ServiceException{
	SocialProvider[] socialProviders = new SocialProvider[1];
	socialProviders[0] = SocialProvider.valueOf(provider.toUpperCase());
//	url format must be /share/{accountId}/email/{emaiId}/{subject encoded}
	String[] splitted = url.split("/");
	
	EmailShared sharedEmail = sharingService.saveShareEmail(sessionInfo.getAccount().getRemoteId(), splitted[4], socialProviders);
	return new ActionContentResponse<String>(true, null, sharedEmail.getId());
    }
    
    @RequestMapping(value = "discovery", method = RequestMethod.GET)
    public ActionContentResponse<AccountSourceDiscover> discovery(@RequestParam String email) throws ServiceException {
	return new ActionContentResponse<AccountSourceDiscover>(true, null, accountService.discoverAccountSourceSettings(email));
    }
}