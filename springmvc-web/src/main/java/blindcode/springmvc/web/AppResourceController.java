/**
 * 
 */
package blindcode.springmvc.web;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerMapping;

import blindcode.springmvc.web.common.BaseController;

/**
 * Serve requests directly from the UI mostly angular calls.
 * 
 * @author David
 *
 */
@Controller
@RequestMapping(value = "/app/")
public class AppResourceController extends BaseController {

	private static final String TEMPLATE_PATH = "app/";

	
	/**
	 * Return a HTML template indicated as part of the request URI. 
	 * Example: /app/html/myfolder/template, the method will return myfolder/template
	 *   
	 * @param request HTTP request
	 * @return the template to show to the UI
	 * @throws Exception in the case of an invalid template
	 */
	@RequestMapping(value = "html/**", method = { RequestMethod.GET })
	public String get(final HttpServletRequest request) throws Exception {
		String path = (String) request
				.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
		String bestMatchPattern = (String) request
				.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);

		AntPathMatcher apm = new AntPathMatcher();
		String template = apm.extractPathWithinPattern(bestMatchPattern, path);
		if (!validTemplate(template)){
			throw new Exception("Not valid call to: " + template);
		}
		return TEMPLATE_PATH + template;
	}

	private boolean validTemplate(String template) {
		return StringUtils.isNotBlank(template) && !"..".contains(template);
	}
}
