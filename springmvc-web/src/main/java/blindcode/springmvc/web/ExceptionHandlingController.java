/**
 * 
 */
package blindcode.springmvc.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import blindcode.springmvc.web.session.SessionInfoResolver;

/**
 * Global controller to handle exceptions
 * @author David
 *
 */
@ControllerAdvice
public class ExceptionHandlingController {

	public static final String DEFAULT_ERROR_VIEW = "error";
	
	@Autowired
	SessionInfoResolver sessionInfoResolver;
	
	protected final String httpStatus500 = HttpStatus.INTERNAL_SERVER_ERROR.value() + " " + HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(); 
	
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = Exception.class)
    public ModelAndView defaultErrorHandler(HttpServletRequest request, HttpServletResponse response, Exception e) throws Exception {
        // If the exception is annotated with @ResponseStatus rethrow it and let
        // the framework handle it - like the OrderNotFoundException example
        // AnnotationUtils is a Spring Framework utility class.
        if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null){
        	throw e;        	
        }
        
        // Otherwise setup and send the user to a default error-view.
        ModelAndView modelAndView = new ModelAndView();
        sessionInfoResolver.postHandle(request, modelAndView);
        modelAndView.addObject("exception", e);
        modelAndView.addObject("url", request.getRequestURL());
        modelAndView.addObject("httpStatus", httpStatus500);
        modelAndView.setViewName(DEFAULT_ERROR_VIEW);
        return modelAndView;
    }
}
