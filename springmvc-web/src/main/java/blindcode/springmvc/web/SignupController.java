package blindcode.springmvc.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import blindcode.springmvc.common.Account;
import blindcode.springmvc.service.AccountService;
import blindcode.springmvc.service.ServiceException;
import blindcode.springmvc.web.common.BaseController;

/**
 * Sign up Controller
 *
 */
@Controller
public class SignupController extends BaseController{

	@Autowired
	private AccountService accountService;
	
    @RequestMapping(value = "/signup")
    public String signup(Model model) {
    	model.addAttribute("account", new Account());
        return "signup";
    }

    @RequestMapping(value = "/signup/now",  method = { RequestMethod.POST })
    public String signupNow(Model model, @Valid Account account, BindingResult binder) throws ServiceException{
    	if (binder.hasErrors()){
    		return "signup";
    	}
    	account.setId(null);
    	account.setActive(true);
    	try {
			accountService.create(account);
		} catch (ServiceException serviceException) {
			logger.error("Exception signing up an account", serviceException);
			return "signup";
		}
    	return "signup_complete";
    }
}