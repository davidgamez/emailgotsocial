/**
 * 
 */
package blindcode.springmvc.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 * Base class for controllers tests
 * 
 * @author David
 *
 */
@WebAppConfiguration
@TestPropertySource(locations ={ "/mvc-dispatcher.properties", "/config/app-core.prop", "/config/app.prop"})
@ContextConfiguration({ "classpath*:config/applicationContext*", "classpath*:config/mvc-dispatcher-servlet.xml"})
public class AbstractContextControllerTests {

    @Autowired
    protected WebApplicationContext wac;

}
