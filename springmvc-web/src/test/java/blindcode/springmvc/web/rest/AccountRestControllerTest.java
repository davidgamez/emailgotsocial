/**
 * 
 */
package blindcode.springmvc.web.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import blindcode.springmvc.web.AbstractContextControllerTests;

/**
 * Testing class for {@link AccountRestController}
 * 
 * @author David
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountRestControllerTest extends AbstractContextControllerTests {

    private MockMvc mockMvc;

    @Before
    public void setup() throws Exception {
	this.mockMvc = webAppContextSetup(this.wac).build();
	User user = new User("guest", "", AuthorityUtils.createAuthorityList("ROLE_GUEST"));
	TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user, null);
	SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
    }

    @Test
    public void addAccount() throws Exception {
//	missing account source properties, expected bad request
	String json = "{ \"email\": \"test@test.com\"" + "}";
	this.mockMvc.perform(post("/rest/account/source").contentType(MediaType.APPLICATION_JSON).content(json))
		.andExpect(status().isBadRequest());
    }

}
