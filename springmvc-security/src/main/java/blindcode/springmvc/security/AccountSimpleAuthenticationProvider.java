/**
 * 
 */
package blindcode.springmvc.security;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import blindcode.springmvc.common.Account;
import blindcode.springmvc.common.AppRole;
import blindcode.springmvc.common.EnvironmentInfo;
import blindcode.springmvc.service.AccountService;
import blindcode.springmvc.service.ServiceException;

/**
 * @author David
 *
 */
public class AccountSimpleAuthenticationProvider implements AuthenticationProvider {

    private final Logger logger = LoggerFactory.getLogger(AccountSimpleAuthenticationProvider.class);

    @Autowired
    AccountService accountService;
    
    @Autowired
    EnvironmentInfo environmentInfo;

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.security.authentication.AuthenticationProvider#
     * authenticate(org.springframework.security.core.Authentication)
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
	String name = authentication.getName();
	String password = authentication.getCredentials().toString();

	Account account = null;
	try {
	    account = accountService.findByEmail(name);
	} catch (Exception e) {
	    throw new AuthenticationServiceException("Unable to authenticate", e);
	}
	if (account == null) {
	    logger.error("Failed authentication attempt, user not found: " + name);
	    throw new BadCredentialsException("Invalid creadentials");
	}
	if (!account.isActive()) {
	    throw new DisabledException("Account disabled");
	}
	boolean validCredentials = false;
	try {
	    validCredentials = authenticateAgainstAccountService(name, password, account);
	} catch (ServiceException e) {
	    throw new AuthenticationServiceException("Unable to authenticate", e);
	}
	if (validCredentials) {
	    List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
	    if (CollectionUtils.isNotEmpty(account.getRoles())) {
		for (AppRole appRole : account.getRoles()) {
		    grantedAuths.add(new SimpleGrantedAuthority(appRole.name()));
		}
	    }
	    environmentInfo.setAccount(account);
	    return new UsernamePasswordAuthenticationToken(name, password, grantedAuths);
	} else {
	    throw new AuthenticationServiceException("Unable to auth against third party systems");
	}
    }

    private boolean authenticateAgainstAccountService(String name, String password, Account account)
	    throws ServiceException {
	boolean result = accountService.chanllengePassword(account, password);
	if (!result) {
	    logger.error("Failed authentication attempt, password doesn't match for user: " + name);
	    throw new BadCredentialsException("Invalid creadentials");
	}
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.springframework.security.authentication.AuthenticationProvider#supports
     * (java.lang.Class)
     */
    @Override
    public boolean supports(Class<?> authentication) {
	return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
